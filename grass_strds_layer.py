# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassRasterLayer
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import *
from strds_settings_dialog import STRDSSettingsDialog
from grass_raster_layer import GrassRasterLayer


################################################################################


class GrassSTRDSLayer(GrassRasterLayer):

    LAYER_TYPE = "GrassSTRDSLayer"

    def __init__(self):
        GrassRasterLayer.__init__(self, GrassSTRDSLayer.LAYER_TYPE)

    def initialize(self, raster_name,
                   mapset, info_text,
                   iface, strds_name):
        super(GrassSTRDSLayer, self).initialize(raster_name, mapset,
                                                info_text, iface)
        self.strds_name = strds_name
        self._set_layer_name()

        self.dlg = STRDSSettingsDialog(iface=iface,
                                       strds_layer=self)

    def _set_layer_name(self):
        self.setLayerName("STRDS:  " + self.strds_name + "\n  map:  " +
                          self.raster_name)

    def change_map(self, raster_name):

        self.raster_name = raster_name

        if self.raster_exist() is True:
            self.info = self.grass_iface.read_raster_info(self.raster_name,
                                                          self.mapset)

            self._set_layer_name()

            # We set the extent from the raster map layer
            self.setExtent(QgsRectangle(self.info["west"],
                                        self.info["south"],
                                        self.info["east"],
                                        self.info["north"]))

            self.layerNameChanged.emit()
            self.legendChanged.emit()
            self.repaintRequested.emit()
            return True
        else:
            QgsMessageLog.logMessage("GrassSTRDSLayer: Raster map layer <%s> does not exist"%self.raster_name)
            return False

    def set_opacity(self, opacity):
        """Set the opacity of the layer

           :param opacity: A value between 0 and 100
        """
        if self.raster_exist() is True:
            self.opacity = opacity / 100.0
            self.repaintRequested.emit()

    def showSTRDSSettings(self):
        self.dlg.show()
        # Run the dialog event loop
        result = self.dlg.exec_()
        # See if OK was pressed
        if result == 1:
            # do something useful (delete the line containing pass and
            # substitute with your code)
            pass


################################################################################


class GrassSTRDSLayerType(QgsPluginLayerType):
    def __init__(self):
        QgsPluginLayerType.__init__(self, GrassSTRDSLayer.LAYER_TYPE)

    def createLayer(self):
        return GrassSTRDSLayer()

    def showLayerProperties(self, layer):
        layer.showSTRDSSettings()
        return True

