# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TimeSeriesSampling
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from qgis.core import *
import grass.script as gscript
import csv
from subprocess import PIPE
from grass.pygrass.modules import Module

# create the dialog for zoom to point
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class TimeSeriesSampling(QtCore.QObject):
    """Time series sampling with t.rast.sample
    """

    abort = QtCore.pyqtSignal()
    # This signal sends the generated data
    finished = QtCore.pyqtSignal(dict, str)

    def __init__(self, strds, settings):
        """
        Constructor for time series computation
        :param strds: The space time raster dataset QGIS layer object
        :param settings: The widget that manages the settings
        :return:
        """
        self.where = None
        self.strds = strds
        self.settings = settings

        self.temp_file = gscript.tempfile(1)

        self.t_rast_sample = Module("t.rast.hample",
                                  output=self.temp_file,
                                  strds=strds.strds_name,
                                  flags="r",
                                  verbose=True,
                                  run_=False,
                                  stdout_=PIPE)


        super(TimeSeriesSampling, self).__init__()

    def __del__(self):
        try:
            self.t_rast_sample.popen.kill()
        except Exception, e:
            self.abort.emit()

    def __exit__(self):
        try:
            self.t_rast_sample.popen.kill()
        except Exception, e:
            self.abort.emit()

    def set_values(self, dialog):
        """
        Set the initial values
        :param dialog:
        :return:
        """
        self.dialog = dialog

    @QtCore.pyqtSlot(QgsPoint, str)
    def compute_stats(self, point, where):
        if self.strds is None:
            self.abort.emit()
            return

        try:
            self.t_rast_sample.popen.kill()
        except Exception, e:
            gscript.error(e)

        try:
            # We need to use a temporary region to compute the statistics
            self.t_rast_sample.inputs.coordinates = point.coords()
            self.t_rast_sample.inputs.where = where
            self.t_rast_sample.run()
        except Exception, e:
            QtGui.QMessageBox.critical(self.dialog, "Error", "Error in t.rast.sample:\n%s"%(str(e)))
            self.abort.emit()
            return

        print(self.t_rast_sample.get_bash())
        self.finished.emit(None, self.temp_file)


class TimeSeriesSamplingWhat(QtCore.QObject):
    """
    Skeleton of a parallel/interruptable point/strds sample class

    Unused at the moment
    """

    abort = QtCore.pyqtSignal()
    # This signal sends the generated data
    finished = QtCore.pyqtSignal(dict, str)

    def __init__(self, strds, settings):
        """
        Constructor for time series computation
        :param strds: The space time raster dataset QGIS layer object
        :param settings: The widget that manages the settings
        :return:
        """
        self.where = None
        self.strds = strds
        self.settings = settings

        self.temp_file = gscript.tempfile(1)

        self.t_rast_what = Module("t.rast.what",
                                  output=self.temp_file,
                                  strds=strds.strds_name,
                                  verbose=True,
                                  run_=False,
                                  stdout_=PIPE)


        super(TimeSeriesSampling, self).__init__()

    def __del__(self):
        try:
            self.t_rast_what.popen.kill()
        except Exception, e:
            self.abort.emit()

    def __exit__(self):
        try:
            self.t_rast_what.popen.kill()
        except Exception, e:
            self.abort.emit()

    def set_values(self, dialog):
        """
        Set the initial values
        :param dialog:
        :return:
        """
        self.dialog = dialog

    @QtCore.pyqtSlot(QgsPoint, str)
    def compute_stats(self, point, where):
        if self.strds is None:
            self.abort.emit()
            return

        try:
            self.t_rast_what.popen.kill()
        except Exception, e:
            gscript.error(e)

        try:
            # We need to use a temporary region to compute the statistics
            gscript.use_temp_region()
            self.t_rast_what.inputs.coordinates = point.coords()
            self.t_rast_what.inputs.where = where
            print(self.t_rast_what.get_bash())
            self.t_rast_what.run()
            gscript.del_temp_region()
        except Exception, e:
            QtGui.QMessageBox.critical(self.dialog, "Error", "Error in t.rast.what:\n%s"%(str(e)))
            self.abort.emit()
            return

        r = csv.reader(open(self.temp_file, "r"), delimiter="|")
        for line in r:
            print(line)

        self.finished.emit(None, self.temp_file)
