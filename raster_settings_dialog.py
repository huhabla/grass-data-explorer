# -*- coding: utf-8 -*-
"""
/***************************************************************************
 RasterSettingsDialog
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from ui_raster_settings import Ui_RasterSettings
import grass.script as gscript
import csv
from grass_sample_tool import GrassSampleTool

# matplotlib stuff
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt

# create the dialog for zoom to point
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class RasterSettingsDialog(QtGui.QDialog, Ui_RasterSettings):
    def __init__(self, iface, raster_layer):
        QtGui.QDialog.__init__(self)

        self.setupUi(self)
        self.iface = iface

        self.canvas = self.iface.mapCanvas()

        self.raster_layer = raster_layer
        self.plainTextEdit.setPlainText(self.raster_layer.info_text)
        self.rasterNameLabel.setText(self.raster_layer.raster_name)
        # Add layer name to window title
        title = self.windowTitle()
        self.setWindowTitle(title + " (%s)"%(self.raster_layer.raster_name))

        # Setup the toolbar
        self.toolBar = QtGui.QToolBar()
        self.verticalLayoutRoot.insertWidget(0, self.toolBar)

        self.pushButtonOpacity.clicked.connect(self.set_opacity)
        self.pushButtonRasterStats.clicked.connect(self.generate_raster_plot)
        self.pushButtonApplyColors.clicked.connect(self.apply_colors)

        # GRASS MapTool
        self.grass_sample_tool = GrassSampleTool(self.canvas)
        self.grass_sample_tool.canvasClicked.connect(self.sample_raster_point)

        # Create action for the raster map samppling
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/SampleRaster.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sampleAction = QtGui.QAction(icon, u"Sample Grass Raster Layer", self)
        # connect the action to the run method
        self.sampleAction.triggered.connect(self.set_grass_map_tool)

        # Add toolbar button and menu item
        self.toolBar.addAction(self.sampleAction)

        self.setup_raster_plot()

        self.listWidget.itemClicked.connect(self.switch_stacked_widget)
        self.listWidget.itemActivated.connect(self.switch_stacked_widget)
        self.listWidget.currentItemChanged.connect(self.switch_stacked_widget)

    def check_raster_layer(self):

        if self.raster_layer.raster_exist() is False:
            QtGui.QMessageBox.critical(self, "Error",
                                       "GRASS raster layer <%s> does not exist"%(self.raster_layer.raster_name))
            return False

        return True

    def switch_stacked_widget(self):
        self.stackedWidget.setCurrentIndex(self.listWidget.currentIndex().row())

        if self.listWidget.currentIndex().row() == 3:
            self.set_grass_map_tool()

    def clean_up(self):
        # Unset the maptool
        self.canvas.unsetMapTool(self.grass_sample_tool)

    def setup_raster_plot(self):
        # Setup the plot widget
        self.figureRaster = plt.figure()
        self.plotCanvasRaster = FigureCanvas(self.figureRaster)
        self.verticalLayoutRasterStats.addWidget(self.plotCanvasRaster)

    def set_grass_map_tool(self):
        if self.check_raster_layer() is True:

            # Switch to sample output tab
            self.stackedWidget.setCurrentIndex(3)
            self.canvas.setMapTool(self.grass_sample_tool)

    def sample_raster_point(self, point):
        """
        Sample the raster layer by a single point
        :param point:
        :return:
        """
        if self.check_raster_layer() is True:
            # Switch to sample output tab
            self.stackedWidget.setCurrentIndex(3)
            lines = self.grass_sample_tool.sample_rasters_by_point(point,
                                                                  [self.raster_layer.raster_name,])
            self.textEditSampling.setText(lines)

    def generate_raster_plot(self):
        """
        Generate the raster histogram plot
        :return:
        """
        if self.check_raster_layer() is True:
            # Generate the data to plot
            self.figureRaster.clear()
            self.raster_ax = self.figureRaster.add_subplot(111)
            self.raster_ax.hold(False)

            raster_id = self.raster_layer.raster_name + "@" + \
                        self.raster_layer.mapset

            temp_file = gscript.tempfile(1)
            nsteps = self.rasterStatsSteps.value()

            # Type of visualization
            text = self.comboBoxRasterStats.currentText()

            flags = "An"

            if text == "Cells":
                flags += "c"
                self.raster_ax.set_ylabel("Number of cells")
            elif text == "Area":
                flags += "a"
                self.raster_ax.set_ylabel("Area")
            elif text == "Percent":
                flags += "p"
                self.raster_ax.set_ylabel("Percent")

            # We need to use a temporary region to compute the statistics
            gscript.use_temp_region()
            if self.checkBoxUseRasterRegion.isChecked() is True:
                gscript.run_command("g.region", rast=raster_id)
            gscript.run_command("r.stats", flags=flags,
                                input=raster_id, sep="|",
                                output=temp_file,
                                overwrite=True,
                                nsteps=nsteps)
            gscript.del_temp_region()

            r = csv.reader(open(temp_file, "r"), delimiter="|")
            x = []
            y = []
            y_min = []

            for e in r:
                x.append(float(e[0].replace("%", "")))
                y.append(float(e[1].replace("%", "")))
                y_min.append(0)

            self.raster_ax.vlines(x, y_min, y, colors="black")

            # refresh canvas
            self.plotCanvasRaster.draw()

    def set_opacity(self):
        if self.check_raster_layer() is True:
            self.raster_layer.set_opacity(self.spinBoxOpacity.value())

    def apply_colors(self):
        """
        Apply the chosen color and call the repaint and layer changed signals
        :return:
        """
        if self.check_raster_layer() is True:
            color = self.treeWidgetColors.currentItem().text(0)
            if color:
                gscript.run_command("r.colors", map=self.raster_layer.raster_name, color=color)
                self.raster_layer.repaintRequested.emit()
                self.raster_layer.legendChanged.emit()