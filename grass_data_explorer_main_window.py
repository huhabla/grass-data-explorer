# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassDataExplorerMainWindow
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers
 and space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui, QtXml, QtWebKit

from cStringIO import StringIO
import sys
import os
import subprocess

from qgis.core import *
from qgis.gui import *
from ui_grass_data_explorer_main_window import Ui_GrassDataExplorer
from grass_vector_to_memory_layer import grass_vector_to_memory_layer
from grass_raster_layer import GrassRasterLayer
from grass_strds_layer import GrassSTRDSLayer
from grass_stvds_layer import GrassSTVDSLayer
import grass.script as gscript
import grass.temporal as tgis
from grass.temporal import CLibrariesInterface
from grass.pygrass.rpc import DataProvider
from grass_sample_tool import GrassSampleTool
from grass_region_to_memory_layer import grass_region_to_memory_layer
from grass_region_to_memory_layer import update_memory_layer_by_region
from xml_parser import GrassModuleReader
from ui_about import Ui_AboutDialog

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class Capturing(list):
    """
    Many thanks to kindall (https://stackoverflow.com/users/416467/kindall)
    for this great stdout capture class described in:
    https://stackoverflow.com/questions/16571150/how-to-capture-stdout-output-from-a-python-function-call
    """
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        sys.stdout = self._stdout

class AboutDialog(QtGui.QDialog, Ui_AboutDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi(self)

class GrassDataExplorerMainWindow(QtGui.QMainWindow, Ui_GrassDataExplorer):
    """
    The main dialog class of the Grass Data Explorer
    """

    def __init__(self, iface):
        QtGui.QDialog.__init__(self)
        # Build the GUI
        self.setupUi(self)
        # Set the interface and its canvas
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        # Temporal database connection
        self.init_grass()

        # Internal storage of layers
        self.layers = {}
        self.stds_sync_layers = []

        # Store some data about the strds and stvds
        self.strds = {}
        self.stvds = {}

        # The region layer to show the GRASS region in the MapCanvas
        self.region_layer = None
        self.region_shown = False

        # Raster preview image
        self.image = None

        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)

        # The GRASS installation path
        self.gisbase = os.environ["GISBASE"]

        # Path to the python script to generate the GRASS mdoule GUI's
        self.forms_generation_path = os.path.join(self.gisbase,
                                                  "gui",
                                                  "wxpython",
                                                  "gui_core",
                                                  "forms.py")

        # Mutex to call the Grass interface without interruption
        self.mutex = QtCore.QMutex()

        # Index of the sample tool stacked widget
        self.sample_tool_index = 5

        # Connect signal with slots
        self.loadRasterButton.released.connect(self.load_grass_raster_layer)
        self.loadStrdsRasterButton.released.connect(self.load_grass_strds_raster_layer)
        self.loadVectorButton.released.connect(self.load_grass_vector_layer)
        self.loadStvdsVectorButton.released.connect(self.load_grass_stvds_vector_layer)
        self.pushButtonRefreshInfo.released.connect(self.read_info_fill_widget)

        self.pushButtonSTDSSync.released.connect(self.create_synchronized_map_lists)

        self.treeWidgetSTDSSync.itemClicked.connect(self.stds_sync_load)

        self.treeWidgetSTDSSync.itemActivated.connect(self.stds_sync_load)
        self.treeWidgetSTDSSync.currentItemChanged.connect(self.stds_sync_load)

        self.pushButtonGenerateImageStack.released.connect(self.generate_image_stack)
        self.toolButtonLoadPrintTemplate.released.connect(self.load_print_template)
        self.toolButtonSeImagePath.released.connect(self.load_render_image_path)

        self.pushButtonReload.released.connect(self.reload_all)

        self.pushButtonListRaster.released.connect(self.list_raster_maps)
        self.pushButtonListVector.released.connect(self.list_vector_maps)
        self.pushButtonListSTRDS.released.connect(self.list_strds_maps)
        self.pushButtonListSTVDS.released.connect(self.list_stvds_maps)
        self.pushButtonLoadCurrentGRASSRegion.released.connect(self.load_current_region_to_widget)
        self.pushButtonLoadRegionFromCanvas.released.connect(self.load_region_from_canvas_to_widget)
        self.pushButtonLoadRegionFromLayer.released.connect(self.load_region_from_active_layer_to_widget)
        self.pushButtonSetRegion.released.connect(self.set_region_from_widget)

        # Module tree signals
        self.pushButtonSearchModule.released.connect(self.create_module_tree)
        self.lineEditSearchModule.textChanged.connect(self.create_module_tree)
        self.checkBoxCheckKeywords.stateChanged.connect(self.create_module_tree)
        self.checkBoxCheckDescription.stateChanged.connect(self.create_module_tree)

        self.lineEditRasterPattern.returnPressed.connect(self.list_raster_maps)
        self.lineEditVectorPattern.returnPressed.connect(self.list_vector_maps)
        self.lineEditSTRDSWhere.returnPressed.connect(self.list_strds_maps)
        self.lineEditSTVDSWhere.returnPressed.connect(self.list_stvds_maps)

        # Map layer list signals
        self.treeWidgetRaster.itemClicked.connect(self.raster_name_selected)
        self.treeWidgetRaster.itemClicked.connect(self.show_raster_info)
        self.treeWidgetRaster.itemActivated.connect(self.show_raster_info)
        self.treeWidgetRaster.currentItemChanged.connect(self.show_raster_info)

        self.treeWidgetStrds.itemClicked.connect(self.strds_raster_name_selected)
        self.treeWidgetStrds.itemClicked.connect(self.show_strds_raster_info)
        self.treeWidgetStrds.itemActivated.connect(self.show_strds_raster_info)
        self.treeWidgetStrds.currentItemChanged.connect(self.show_strds_raster_info)

        self.treeWidgetStrds.itemClicked.connect(self.strds_name_selected)
        self.treeWidgetStrds.itemClicked.connect(self.show_strds_info)
        self.treeWidgetStrds.itemActivated.connect(self.show_strds_info)
        self.treeWidgetStrds.currentItemChanged.connect(self.show_strds_info)

        self.treeWidgetVector.itemClicked.connect(self.vector_name_selected)
        self.treeWidgetVector.itemClicked.connect(self.show_vector_info)
        self.treeWidgetVector.itemActivated.connect(self.show_vector_info)
        self.treeWidgetVector.currentItemChanged.connect(self.show_vector_info)

        self.treeWidgetStvds.itemClicked.connect(self.stvds_vector_name_selected)
        self.treeWidgetStvds.itemClicked.connect(self.show_stvds_vector_info)
        self.treeWidgetStvds.itemActivated.connect(self.show_stvds_vector_info)
        self.treeWidgetStvds.currentItemChanged.connect(self.show_stvds_vector_info)

        self.treeWidgetStvds.itemClicked.connect(self.stvds_name_selected)
        self.treeWidgetStvds.itemClicked.connect(self.show_stvds_info)
        self.treeWidgetStvds.itemActivated.connect(self.show_stvds_info)
        self.treeWidgetStvds.currentItemChanged.connect(self.show_stvds_info)

        self.treeWidgetGRASSModules.itemClicked.connect(self.module_name_selected)
        self.treeWidgetGRASSModules.itemDoubleClicked.connect(self.module_name_selected_by_doubleclick)

        self.comboBoxSwitchMapset.activated.connect(self.switch_mapset)

        # GRASS MapTool for raster and STRDS-raster sampling
        self.grass_sample_tool = GrassSampleTool(self.canvas)
        self.grass_sample_tool.canvasClicked.connect(self.sample_raster_point)

        # Create action for the raster map sampling
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/SampleRaster.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sampleAction = QtGui.QAction(icon, u"Sample all Grass Raster Layer located in the Layer Tree", self)
        # connect the action to the run method
        self.sampleAction.triggered.connect(self.set_grass_map_tool)
        # Add toolbar button and menu item
        self.toolBar.addAction(self.sampleAction)

        # Create action to show the region
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/showRegion.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.showRegionAction = QtGui.QAction(icon, u"Show/Update current Grass region", self)
        # connect the action to the run method
        self.showRegionAction.triggered.connect(self.show_update_region)
        # Add toolbar button and menu item
        self.toolBar.addAction(self.showRegionAction)

        # Register the layer remove signal
        QgsMapLayerRegistry.instance().layersWillBeRemoved.connect(self.remove_layers)

        # Create action to hide the region
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/hideRegion.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.hideRegionAction = QtGui.QAction(icon, u"Hide current Grass region", self)
        # connect the action to the run method
        self.hideRegionAction.triggered.connect(self.hide_region)
        # Add toolbar button and menu item
        self.toolBar.addAction(self.hideRegionAction)

        self.actionAbout.triggered.connect(self.show_about)
        self.actionHelp.triggered.connect(self.show_help)

        # Switching the stack widgets
        self.listWidget.itemClicked.connect(self.switch_stacked_widget)
        self.listWidget.itemActivated.connect(self.switch_stacked_widget)
        self.listWidget.currentItemChanged.connect(self.switch_stacked_widget)

        # Setup the context menues
        self.setup_context_menues()

        # Set the HTML viewer
        self.moduleHTMLHelp = QtWebKit.QWebView()
        self.verticalLayoutWebFrame.addWidget(self.moduleHTMLHelp)

    def switch_stacked_widget(self):
        """
        Switch between the stacked widgets
        :return:
        """
        self.stackedWidget.setCurrentIndex(self.listWidget.currentIndex().row())

        if self.listWidget.currentIndex().row() == self.sample_tool_index:
            self.set_grass_map_tool()

    def show_about(self):
        """
        Show the about dialog
        :return:
        """
        d = AboutDialog()
        d.exec_()

    def show_help(self):
        """Display application help to the user."""
        help_file = 'file:///%s/help/html/index.html' % self.plugin_dir
        # For testing path:
        #QMessageBox.information(None, 'Help File', help_file)
        # noinspection PyCallByClass,PyTypeChecker
        QtGui.QDesktopServices.openUrl(QtCore.QUrl(help_file))

    ####################################################################
    ################# ANIMATION ########################################
    ####################################################################

    def list_stds_in_sync_tab(self):
        """
        List all STRDS and STVDS layer in the list widget
        of the synchronize tab
        :return:
        """

        self.listWidgetSTDSSync.clear()

        for layer in self.layers.values():
            if isinstance(layer, GrassSTRDSLayer) is True:
                item = QtGui.QListWidgetItem(layer.strds_name, self.listWidgetSTDSSync)
                item.setData(QtCore.Qt.UserRole, layer)
                self.listWidgetSTDSSync.addItem(item)

            if isinstance(layer, GrassSTVDSLayer) is True:
                item = QtGui.QListWidgetItem(layer.stvds_name, self.listWidgetSTDSSync)
                item.setData(QtCore.Qt.UserRole, layer)
                self.listWidgetSTDSSync.addItem(item)

    def create_synchronized_map_lists(self):
        """
        Compute the common granularity of all select STDS and
        sample the map list with the common granularity.
        List the sampled map list synchronously of all selected STDS in
        the tree widget
        :return:
        """

        # Get all selected list items
        self.treeWidgetSTDSSync.clear()

        # Order only selected STDS layers
        item_list = self.listWidgetSTDSSync.selectedItems()
        self.dbif.connect()

        granularities = []
        start_times = []
        map_lists = {}
        valid_layers = []

        temp_type = None

        for item in item_list:
            layer = item.data(QtCore.Qt.UserRole)
            if isinstance(layer, GrassSTRDSLayer) is True:
                stds = tgis.open_old_stds(layer.strds_name, "strds",
                                          self.dbif)
            if isinstance(layer, GrassSTVDSLayer) is True:
                stds = tgis.open_old_stds(layer.stvds_name, "stvds",
                                          self.dbif)
            stds.select(self.dbif)

            where = self.lineEditSTDSSyncWhere.text()

            try:
                # TODO: Enable where statement
                maps = stds.get_registered_maps_as_objects(where=where,
                                                           order="start_time",
                                                           dbif=self.dbif)
            except Exception, e:
                QtGui.QMessageBox.critical(self, "Error", "Error in SQL:\n%s"%(str(e)))
                return

            map_lists[layer.id()] = maps

            # Jump over empty
            if len(maps) == 0:
                continue

            # Check if the stds have a valid temporal topology
            if stds.check_temporal_topology(maps, self.dbif) is False:
                raise Exception("Temporal topology of STDS is invalid")

            # Read the granularity
            granularities.append(stds.get_granularity())
            start_times.append(stds.get_temporal_extent_as_tuple()[0])

            # Check the temporal type
            if temp_type is None:
                temp_type = stds.get_temporal_type()
            else:
                if temp_type != stds.get_temporal_type():
                    raise Exception("STDS to snc have different temporal types")

            valid_layers.append(layer)

        if len(valid_layers) == 0:
            return

        # TODO: The granularity computation can not be used for synchronization
        # if the start time of a STDS will intersect the next higher calendar
        # hierarchy when a multiple of the common granularity is added
        # -> metereological seasons -> 3 months granularity but start at 1. March
        # and intersects the year border
        #print("Granularities", granularities)

        if temp_type == "absolute":
            common_gran = tgis.compute_common_absolute_time_granularity(granularities,
                                                                        start_times)

        if temp_type == "relative":
            common_gran = tgis.compute_common_relative_time_granularity(granularities,
                                                                        start_times)

        map_table = {}
        ordered_time_table = []
        valid_layer_names = []

        for layer in valid_layers:
            if isinstance(layer, GrassSTRDSLayer) is True:
                stds = tgis.open_old_stds(layer.strds_name, "strds",
                                          self.dbif)
                valid_layer_names.append(layer.strds_name)
            if isinstance(layer, GrassSTVDSLayer) is True:
                stds = tgis.open_old_stds(layer.stvds_name, "stvds",
                                          self.dbif)
                valid_layer_names.append(layer.stvds_name)

            maps = map_lists[layer.id()]

            start_a, end_a = maps[0].get_temporal_extent_as_tuple()
            start_b, end_b = maps[-1].get_temporal_extent_as_tuple()

            start = start_a
            end = end_b
            if end is None:
                end = start_b

            sampled_maps = stds.resample_maplist_by_granularity(maps, start, end, common_gran)

            for map_ in sampled_maps:
                start, end = map_[0].get_temporal_extent_as_tuple()

                # First entry in the table
                if str(start) not in map_table:
                    map_table[str(start)] = {}
                    ordered_time_table.append(start)
                    map_table[str(start)][layer.id()] = map_[0]
                else:
                    map_table[str(start)][layer.id()] = map_[0]

        # Create the tree widget entries and sort by start time
        ordered_time_table.sort()

        self.treeWidgetSTDSSync.clear()
        self.treeWidgetSTDSSync.setColumnCount(len(valid_layers) + 1)
        self.treeWidgetSTDSSync.setHeaderLabels(["Start time"] + valid_layer_names)

        # Fill the tree widget with items
        count = 0
        for start in ordered_time_table:
            entries = map_table[str(start)]

            item = QtGui.QTreeWidgetItem()
            item.setText(0, str(start))

            for layer in valid_layers:
                if layer.id() in entries:
                    index = valid_layers.index(layer)
                    map_ = entries[layer.id()]
                    #print(layer.id(), map_.get_name(), index + 1)
                    item.setText(index + 1, map_.get_name())
                    start, end = map_.get_temporal_extent_as_tuple()
                    item.setData(0, QtCore.Qt.UserRole, [start, end])

            self.treeWidgetSTDSSync.insertTopLevelItem(count, item)
            count += 1

        self.stds_sync_layers = valid_layers

        for column in range(len(valid_layers) + 1):
            self.treeWidgetSTDSSync.resizeColumnToContents(column)

        self.labelCommonGranularity.setText("Common granularity is: %s"%str(common_gran))

        self.dbif.close()

    def stds_sync_load(self, item, selected_column):
        """Load the selected maps of the synced STDS layer
        """
        if item is not None:
            # We need to unset the visibility of empty layers
            # separately
            unconsidered_layers = []
            unconsidered_layers.extend(self.stds_sync_layers)

            legend = self.iface.legendInterface()

            # The column count may stop to early, if
            # the last columns are empty
            for column in range(item.columnCount() - 1):
                layer = self.stds_sync_layers[column]
                unconsidered_layers.remove(layer)

                name = item.text(column + 1)

                # Check for empty layers
                if name:
                    legend.setLayerVisible(layer, True)
                    if isinstance(layer, GrassSTRDSLayer) is True:
                        layer.change_map(name)
                    if isinstance(layer, GrassSTVDSLayer) is True:
                        layer.change_map(name,
                                         layer.feat_type,
                                         layer.add_attr,
                                         layer.bbox)
                else:
                    legend.setLayerVisible(layer, False)

            # Check for unconsidered layers
            for layer in unconsidered_layers:
                legend.setLayerVisible(layer, False)

    def generate_image_stack(self):
        """
        Generate the image stack from the synchronized space-time datasets
        :return:
        """

        template_path = self.lineEditPrintTemplate.text()
        image_path = self.lineEditImagePath.text()

        if not template_path:
            QtGui.QMessageBox.critical(self, "Error no template file specified",
                                       "You need to specify a valid template file.")
            return

        if not image_path:
            QtGui.QMessageBox.critical(self, "Error no image path specified",
                                       "You need to specify a valid image path.")
            return

        if self.treeWidgetSTDSSync.topLevelItemCount() == 0:
            QtGui.QMessageBox.critical(self, "Error no synchronized time series",
                                       "Please synchronize at least one time series.")
            return

        template_file = file(template_path)
        template_content = template_file.read()
        template_file.close()

        self.progressBarImageGeneration.setMinimum(0)
        self.progressBarImageGeneration.setMaximum(100)
        self.progressBarImageGeneration.setValue(0)
        self.textEditRenderinfo.clear()

        # Iterate over all synced layers
        count = 0
        iterator = QtGui.QTreeWidgetItemIterator(self.treeWidgetSTDSSync)
        num_items = self.treeWidgetSTDSSync.topLevelItemCount()

        while iterator.value():

            # Set progress
            progress = int(float(count) / float(num_items) * 100)
            self.progressBarImageGeneration.setValue(progress)

            item = iterator.value()
            data = item.data(0, QtCore.Qt.UserRole)
            start = data[0]
            end = data[1]

            if end is None:
                end = start

            # Load the required layers
            self.stds_sync_load(item, 0)
            count += 1
            iterator += 1

            c = QgsComposition(self.iface.mapCanvas().mapRenderer())
            c.setPlotStyle(QgsComposition.Print)

            document = QtXml.QDomDocument()
            document.setContent(template_content)
            substitution_map = {'START_DATE': '%s'%str(start),
                                'END_DATE': '%s'%str(end),
                                'START_YEAR' : "%04i"%start.year,
                                'START_MONTH' : "%02i"%start.month,
                                'START_DAY' : "%02i"%start.day,
                                'START_HOUR' : "%02i"%start.hour,
                                'START_MINUTE' : "%02i"%start.minute,
                                'START_SECOND' : "%02i"%start.second,
                                'END_YEAR' : "%04i"%end.year,
                                'END_MONTH' : "%02i"%end.month,
                                'END_DAY' : "%02i"%end.day,
                                'END_HOUR' : "%02i"%end.hour,
                                'END_MINUTE' : "%02i"%end.minute,
                                'END_SECOND' : "%02i"%end.second}
            c.loadFromTemplate(document, substitution_map)
            c.refreshItems()

            # Compute the image resolution
            dpi = c.printResolution()
            dpmm = dpi / 25.4
            width = int(dpmm * c.paperWidth())
            height = int(dpmm * c.paperHeight())

            # create output image and initialize it
            image = QtGui.QImage(QtCore.QSize(width, height),
                                 QtGui.QImage.Format_ARGB32)
            image.setDotsPerMeterX(dpmm * 1000)
            image.setDotsPerMeterY(dpmm * 1000)
            image.fill(0)

            # render the composition
            imagePainter = QtGui.QPainter(image)
            sourceArea = QtCore.QRectF(0, 0, c.paperWidth(),
                                       c.paperHeight())
            targetArea = QtCore.QRectF(0, 0, width, height)

            c.render(imagePainter, targetArea, sourceArea)
            imagePainter.end()
            try:
                image_file = "%s_%05i.png"%(image_path, count)
                f = open(image_file, "w")
                f.close()
                image.save("%s"%image_file, "png")
            except IOError, e:
                QtGui.QMessageBox.critical(self, "Error writing image",str(e))
                return

            # Fill the information widgets
            content =  "Save image as......: %s\n"%image_file
            content += "Start time.........: %s\n"%str(data[0])
            content += "End time...........: %s\n"%str(data[1])
            content += "Rendered GRASS layers:\n"
            for i in range(item.columnCount()):
                if i + 1 == item.columnCount():
                    break
                content += "  %s\n"%item.text(i + 1)

            self.textEditRenderinfo.setText(content)

            preview_pixmap = QtGui.QPixmap.fromImage(image.scaled(400, 300,
                                                                  QtCore.Qt.KeepAspectRatio))
            self.renderPreviewLabel.setPixmap(preview_pixmap)
            self.update()

        self.progressBarImageGeneration.setValue(100)

    def load_print_template(self):
        file_name = QtGui.QFileDialog.getOpenFileName(self,
                                                      "Open print template",
                                                      "",
                                                      "Qgis print template files (*.qpt)")
        self.lineEditPrintTemplate.setText(file_name)

    def load_render_image_path(self):
        image_path = QtGui.QFileDialog.getSaveFileName(self, "Select an image basename path",
                                                      "",
                                                      "Path and basename to the generated image files (*.*)")
        self.lineEditImagePath.setText(image_path)

    ####################################################################
    ################# CLEAN UP AND ACTIONS #############################
    ####################################################################

    def __exit__(self, *args):
        self.clean_up()

    def __del__(self, *args):
        self.clean_up()

    def clean_up(self):
        self.canvas.unsetMapTool(self.grass_sample_tool)
        self.remove_all_layers()
        self.stop_grass()

    def stop_grass(self):
        """
        Stop the subprocesses of several grass services
        :return:
        """
        message = "GrassDataExplorerMainWindow: stop_grass: Stop GRASS provider processes"
        #QgsMessageLog.logMessage(message)
        sys.stderr.write(message + "\n")
        if self.grass_iface:
            self.grass_iface.stop()
        if self.grass_data_provider:
            self.grass_data_provider.stop()
        tgis.stop_subprocesses()

    def remove_all_layers(self):
        """
        Remove all layers from the internal and QGIS layer registry
        :return:
        """
        for layer in self.layers.values():
            # Sometimes layers are remoced with notification
            try:
                layer.id()
            except RuntimeError, e:
                continue
            message = "GrassDataExplorerMainWindow: remove_all_layers: Delete layer id %s of type %s\n"%(layer.id(),
                                                                                  type(layer))
            #QgsMessageLog.logMessage(message)
            sys.stderr.write(message + "\n")
            layer.clean_up()
            self.layers.pop(layer.id())
            # Catch RuntimeError that occurs if QGIS has already deleted
            # this layer
            try:
                QgsMapLayerRegistry.instance().removeMapLayer(layer.id())
            except RuntimeError:
                pass

        self.layers = {}

        if self.region_layer is not None:
            # Catch RuntimeError that occurs if QGIS has already deleted
            # this layer
            try:
                QgsMapLayerRegistry.instance().removeMapLayer(self.region_layer.id())
                self.region_layer = None
            except RuntimeError:
                pass

        self.list_stds_in_sync_tab()

    @QtCore.pyqtSlot()
    def hide_region(self):
        """
        Remove the region layer from the layer registry
        :return:
        """

        # We need to check if the layer was deleted by qgis
        if self.region_layer is not None:
            try:
                self.region_layer.id()
            except RuntimeError:
                self.region_layer = None

        if self.region_layer is not None:
            QgsMapLayerRegistry.instance().removeMapLayer(self.region_layer.id())
        self.region_layer = None

    @QtCore.pyqtSlot()
    def show_update_region(self, update_only=False):
        """
        Show the current GRASS region as memory layer or update this layer
        if the region is already shown.

        :param update_only: If set True the region will only be updated
                            if it is already visible as layer
        :return:
        """
        if self.region_layer is None and update_only is True:
            return

        # We need to check if the layer was deleted by qgis
        if self.region_layer is not None:
            try:
                self.region_layer.id()
            except RuntimeError:
                self.region_layer = None

        if self.region_layer is None:
            self.region_layer = grass_region_to_memory_layer()
            QgsMapLayerRegistry.instance().addMapLayer(self.region_layer)
        else:
            grass_region = gscript.region()
            update_memory_layer_by_region(self.region_layer, grass_region)

        # Load and set the default region style
        filename = os.path.join(self.plugin_dir, "default styles", "grass_region.qml")
        self.region_layer.loadNamedStyle(filename)

    def init_grass(self):
        """
        Initiate the temporal framework, read available mapsets,
        start the grass service subprocesses and establish a temporal
        database connection for efficient temporal processing.
        :return:
        """
        QgsMessageLog.logMessage("GrassDataExplorerMainWindow: init_grass: Initiate GRASS provider processes")
        # Create the GRASS interfaces
        tgis.init(True)

        self.current_mapset = gscript.read_command("g.mapset", flags="p").strip()
        self.mapsets = gscript.read_command("g.mapset", flags="l")
        self.mapsets = self.mapsets.split()
        self.accessable_mapsets = gscript.read_command("g.mapsets", flags="p")
        self.accessable_mapsets = self.accessable_mapsets.split()

        self.grass_iface = CLibrariesInterface()
        self.grass_data_provider = DataProvider()

        self.dbif = tgis.SQLDatabaseInterfaceConnection()

    @QtCore.pyqtSlot()
    def switch_mapset(self):
        """
        Switch the current mapset to the one selected in the
        mapset combo box

        This function switches the GRASS mapset. It will stop
        all subprocesses, remove all GRASS raster and strds
        layers from QGIS, close layer dialogs,
        clear all information in the GUI and re-read it for the new
        selected mapset.

        If the current mapset is equal to the selected mapset then
        nothing is done.

        :return:
        """

        # A (new) mapset was selected
        new_mapset = self.comboBoxSwitchMapset.currentText().strip()
        # Do nothin if the current mapset was selected
        if new_mapset == self.current_mapset.strip():
            return

        # Clean all data and stop grass processes
        self.clean_up()

        QgsMessageLog.logMessage("GrassDataExplorerMainWindow: switch_mapset: Switching mapset to %s"%new_mapset)
        # Switch the mapset
        gscript.run_command("g.mapset", mapset=new_mapset)

        # Initiate all
        self.init_grass()
        # Load all data
        self.load_all()

    def list_mapsets(self):
        """
        List all mapset in the mapset combo box
        :return:
        """

        self.comboBoxSwitchMapset.clear()
        self.comboBoxSwitchMapset.addItems(self.mapsets)
        index = self.mapsets.index(self.current_mapset)
        self.comboBoxSwitchMapset.setCurrentIndex(index)

    def remove_layers(self, layerIds):
        """
        Remove layers from the internal layer registry
        :param layerIds: A list of ids to remove from the internal layer list
        """
        for layerId in layerIds:
            if layerId in self.layers:
                layer = self.layers[layerId]
                layer.clean_up()
                message = "GrassDataExplorerMainWindow: remove_layers: " \
                          "Delete layer id %s of type %s\n"%(layerId, type(layer))
                QgsMessageLog.logMessage(message)
                sys.stderr.write(message + "\n")
                self.layers.pop(layerId)

        # Update the list of layers in the synchronization tab
        self.list_stds_in_sync_tab()
        # Clear any synced STDS layers
        self.treeWidgetSTDSSync.clear()

    def reload_all(self):
        """
        Restart the grass support processe  and
        show all maps and space time datasets in the tree widgets
        """
        QgsMessageLog.logMessage("GrassDataExplorerMainWindow: reload_all: Reloading GRASS provider processes")
        self.stop_grass()
        self.init_grass()
        self.load_all()

    def load_all(self):
        """
        Show all maps and space time datasets in the tree widgets
        """
        self.list_mapsets()
        self.list_raster_maps()
        self.list_vector_maps()
        self.list_strds_maps()
        self.list_stvds_maps()
        self.read_info_fill_widget()
        # Update the list of layers in the synchronization tab
        self.list_stds_in_sync_tab()
        # Clear any synced STDS layers
        self.treeWidgetSTDSSync.clear()
        # Load all GRASS modules from XML definition file
        url = os.path.join(self.gisbase, "gui", "wxpython", "xml", "module_items.xml")
        xml = GrassModuleReader(url)
        self.modules_keyword_dict = xml.modules_keyword_dict
        self.create_module_tree()
        self.load_current_region_to_widget()

    ####################################################################
    ################# RASTER SAMPLING BY POINT #########################
    ####################################################################

    def set_grass_map_tool(self):
        """
        Set the grass sample MapTool and switch to the
        output tab.
        :return:
        """
        self.stackedWidget.setCurrentIndex(self.sample_tool_index)
        self.listWidget.setItemSelected(self.listWidget.item(self.sample_tool_index), True)
        self.canvas.setMapTool(self.grass_sample_tool)

    def sample_raster_point(self, point):
        """
        Call the raster sample method from the grass sample tool
        and switch to the output tab.
        :param point: QgsPoint of coordinates in the GRASS location coordinate system
        :return:
        """
        raster_names = []
        for layer in self.layers.values():
            if isinstance(layer, GrassSTVDSLayer) is False:
                raster_names.append(layer.raster_name)

        # Switch to sample output tab
        self.stackedWidget.setCurrentIndex(self.sample_tool_index)
        self.listWidget.setItemSelected(self.listWidget.item(self.sample_tool_index), True)
        lines = self.grass_sample_tool.sample_rasters_by_point(point,
                                                               raster_names)
        self.textEditRasterSampling.setText(lines)

    ####################################################################
    ################# LOAD MAPS AS LAYERS ##############################
    ####################################################################

    def load_grass_strds_raster_layer(self):
        """Load a raster layer from GRASS selected in the STRDS dialog
        """
        mapset = None
        dataset_name = self.strdsRasterLayerLine.text()

        if not dataset_name:
            return

        if "STRDS::" in dataset_name:
            dummy, dataset_name = dataset_name.split("::", 1)
            self._load_grass_strds(dataset_name)
            return

        if "@" in dataset_name:
            dataset_name, mapset = dataset_name.split("@")

        self._load_grass_raster_layer(dataset_name, mapset)

    def load_grass_raster_layer(self):
        """Load a raster layer from GRASS selected in the Raster dialog
        """
        mapset = None
        raster_name = self.rasterLayerLine.text()

        if not raster_name:
            return

        if "@" in raster_name:
            raster_name, mapset = raster_name.split("@")

        self._load_grass_raster_layer(raster_name, mapset)

    def _load_grass_raster_layer(self, raster_name, mapset):
        """This function will create a GrassRasterlayer object
           and adds it to the layer list of the QgsMapLayerRegistry
           instance and adds it to the internal layer dictionary.

           In case a layer was removed from the QgsMapLayerRegistry,
           then the entry must be removed from the internal
           layer dict.
        """
        geom = self.rasterImageLabel.size()
        info_text, image = self._generate_raster_info(raster_name,
                                                      mapset,
                                                      geom)

        if info_text is None or image is None:
            return

        layer = GrassRasterLayer()
        layer.initialize(raster_name=raster_name,
                         mapset=mapset,
                         info_text=info_text,
                         iface=self.iface)

        # We need to store a reference to the new layer
        self.layers[layer.id()] = layer

        QgsMapLayerRegistry.instance().addMapLayers([layer])
        self.iface.setActiveLayer(layer)

    def _load_grass_strds(self, strds_name):
        """This function will create a GrassRasterlayer object
           and adds it to the layer list of the QgsMapLayerRegistry
           instance and adds it to the internal layer dictionary.

           In case a layer was removed from the QgsMapLayerRegistry,
           then the entry must be removed from the internal
           layer dict.
        """
        if strds_name in self.strds:
            raster_name = self.strds[strds_name]["first_map"]
            # STRDS is empty
            if raster_name is None:
                return
            mapset = self.strds[strds_name]["mapset"]
        else:
            return

        info_text = self._generate_stds_info(strds_name, "strds")

        if info_text:
            layer = GrassSTRDSLayer()
            layer.initialize(raster_name=raster_name,
                             mapset=mapset,
                             info_text=info_text,
                             iface=self.iface,
                             strds_name=strds_name)

            # We need to store a reference to the new layer
            self.layers[layer.id()] = layer

            QgsMapLayerRegistry.instance().addMapLayers([layer])
            self.iface.setActiveLayer(layer)
            self.list_stds_in_sync_tab()

    def load_grass_stvds_vector_layer(self):
        """
        Load a vector layer from GRASS selected in the STVDS dialog
        """

        mapset = None
        dataset_name = self.stvdsVectorLayerLine.text()

        if not dataset_name:
            return

        if "STVDS::" in dataset_name:
            dummy, dataset_name = dataset_name.split("::", 1)
            self._load_grass_stvds(dataset_name)
            return

        if "@" in dataset_name:
            dataset_name, mapset = dataset_name.split("@")

        add_attr = self.loadStvdsAttributesCheckBox.isChecked()
        use_extent = self.currentStvdsExtentCheckBox.isChecked()
        feat_type = self.stvdsVectorTypeChooser.currentText()

        self._load_grass_vector_layer(dataset_name, mapset,
                                      feat_type, add_attr,
                                      use_extent)

    def load_grass_vector_layer(self):
        """
        Load a vector layer from GRASS selected in the Vector dialog
        """
        mapset = None
        vector_name = self.vectorLayerLine.text()

        if not vector_name:
            return

        if "@" in vector_name:
            vector_name, mapset = vector_name.split("@")

        add_attr = self.loadAttributesCheckBox.isChecked()
        use_extent = self.currentExtentCheckBox.isChecked()
        feat_type = self.vectorTypeChooser.currentText()

        self._load_grass_vector_layer(vector_name, mapset,
                                      feat_type, add_attr,
                                      use_extent)

    def _load_grass_vector_layer(self, vector_name, mapset, feat_type,
                                 add_attr=False, use_extent=False):
        """
        Create a memory layer from a GRASS vector map layer for
        a specific feature type and add it to the QgsMapLayerRegistry.
        The layer will be set as active layer.

        This function supports the selection of a specific extent.
        :param vector_name: Name of the GRASS vector map layer
        :param mapset: The name of the vector map mapset
        :param feat_type: The feature type to use for reading the vector layer
        :param add_attr: If True the vector attributes are generated
        :param use_extent: Clip the vector map layer using the current extent
        :return:
        """
        bbox = None
        if use_extent is True:
            extent = self.iface.mapCanvas().extent()
            bbox = {"north": extent.yMaximum(), "south": extent.yMinimum(),
                    "east": extent.xMaximum(), "west": extent.xMinimum()}

        layer = grass_vector_to_memory_layer(self.grass_data_provider,
                                             self.grass_iface,
                                             self.iface, vector_name, mapset,
                                             feat_type=feat_type,
                                             add_attr=add_attr,
                                             bbox=bbox)
        if layer:
            QgsMapLayerRegistry.instance().addMapLayers([layer])
            self.iface.setActiveLayer(layer)

    def _load_grass_stvds(self, stvds_name):
        """This function will create a GrassSTVDSlayer object
           and adds it to the layer list of the QgsMapLayerRegistry
           instance and adds it to the internal layer dictionary.

           In case a layer was removed from the QgsMapLayerRegistry,
           then the entry must be removed from the internal
           layer dict.
        """
        if stvds_name in self.stvds:
            vector_name = self.stvds[stvds_name]["first_map"]
            # STVDS is empty
            if vector_name is None:
                return
            mapset = self.stvds[stvds_name]["mapset"]
        else:
            return

        add_attr = self.loadStvdsAttributesCheckBox.isChecked()
        use_extent = self.currentStvdsExtentCheckBox.isChecked()

        bbox = None
        if use_extent is True:
            extent = self.iface.mapCanvas().extent()
            bbox = {"north": extent.yMaximum(), "south": extent.yMinimum(),
                    "east": extent.xMaximum(), "west": extent.xMinimum()}

        feat_type = self.stvdsVectorTypeChooser.currentText()
        info_text = self._generate_stds_info(stvds_name, "stvds")

        if info_text:
            layer = GrassSTVDSLayer()
            layer.initialize(vector_name=vector_name,
                             mapset=mapset,
                             info_text=info_text,
                             iface=self.iface,
                             stvds_name=stvds_name,
                             feat_type=feat_type,
                             add_attr=add_attr,
                             bbox=bbox)

            # We need to store a reference to the new layer
            self.layers[layer.id()] = layer

            QgsMapLayerRegistry.instance().addMapLayers([layer])
            self.iface.setActiveLayer(layer)
        self.list_stds_in_sync_tab()

    ####################################################################
    ################# CONTEXT MENUES ###################################
    ####################################################################

    def setup_context_menues(self):

        ################ Raster Context Menu ################
        # Define the context menu when pressing the right mouse button on treeWidgetRaster
        self.treeWidgetRaster.customContextMenuRequested.connect(self.on_raster_context_menu)
        self.rasterContextMenu = QtGui.QMenu(self)
        self.rasterContextMenuActionLoad = QtGui.QAction(u"Load", self)
        self.rasterContextMenuActionLoad.triggered.connect(self.call_raster_context_menu_load)
        self.rasterContextMenuActionSetRegion = QtGui.QAction(u"Set region", self)
        self.rasterContextMenuActionSetRegion.triggered.connect(self.call_raster_context_menu_set_region)
        self.rasterContextMenuActionExport = QtGui.QAction(u"Export", self)
        self.rasterContextMenuActionExport.triggered.connect(self.call_raster_context_menu_export)

        self.rasterContextMenu.addAction(self.rasterContextMenuActionLoad)
        self.rasterContextMenu.addAction(self.rasterContextMenuActionSetRegion)
        self.rasterContextMenu.addAction(self.rasterContextMenuActionExport)

        ################ Vector Context Menu ################
        # Define the context menu when pressing the right mouse button on treeWidgetVector
        self.treeWidgetVector.customContextMenuRequested.connect(self.on_vector_context_menu)
        self.vectorContextMenu = QtGui.QMenu(self)
        self.vectorContextMenuAction = QtGui.QAction(u"Load", self)
        self.vectorContextMenuAction.triggered.connect(self.call_vector_context_menu)

        self.vectorContextMenu.addAction(self.vectorContextMenuAction)

        ################ STRDS Context Menu ################
        # Define the context menu when pressing the right mouse button on treeWidgetSTRDS
        self.treeWidgetStrds.customContextMenuRequested.connect(self.on_strds_context_menu)
        self.strdsContextMenu = QtGui.QMenu(self)
        self.strdsContextMenuAction = QtGui.QAction(u"Load", self)
        self.strdsContextMenuAction.triggered.connect(self.call_strds_context_menu)

        self.strdsContextMenu.addAction(self.strdsContextMenuAction)

        ################ STVDS Context Menu ################
        # Define the context menu when pressing the right mouse button on treeWidgetSTRDS
        self.treeWidgetStvds.customContextMenuRequested.connect(self.on_stvds_context_menu)
        self.stvdsContextMenu = QtGui.QMenu(self)
        self.stvdsContextMenuAction = QtGui.QAction(u"Load", self)
        self.stvdsContextMenuAction.triggered.connect(self.call_stvds_context_menu)

        self.stvdsContextMenu.addAction(self.stvdsContextMenuAction)

    ################# RASTER CONTEXT MENUES #############################

    def call_raster_context_menu_load(self):
        item = self.treeWidgetRaster.currentItem()
        success = self.raster_name_selected(item, 0)
        if success is True:
            self.show_raster_info(item, 0)
            self.load_grass_raster_layer()

    def call_raster_context_menu_set_region(self):
        item = self.treeWidgetRaster.currentItem()
        success = self.raster_name_selected(item, 0)
        if success is True:
            try:
                gscript.run_command("g.region", raster=self.rasterLayerLine.text())
                # Update the region specific widgets and layer
                self.show_update_region(update_only=True)
                self.read_info_fill_widget()
            except:
                raise

            QtGui.QMessageBox.information(self, "Success", "Successfully set region")

    def call_raster_context_menu_export(self):
        item = self.treeWidgetRaster.currentItem()
        success = self.raster_name_selected(item, 0)
        if success is True:
            command = "r.out.gdal input=%(map)s"%{"map":self.rasterLayerLine.text()}
            args = [os.environ["GRASS_PYTHON"], self.forms_generation_path, command]

            p = subprocess.Popen(args=args, shell=False)

    def on_raster_context_menu(self, point):
         self.rasterContextMenu.exec_( self.treeWidgetRaster.mapToGlobal(point))

    ################# VECTOR CONTEXT MENUES #############################

    def call_vector_context_menu(self):
        item = self.treeWidgetVector.currentItem()
        self.vector_name_selected(item, 0)
        self.show_vector_info(item, 0)
        self.load_grass_vector_layer()

    def on_vector_context_menu(self, point):
         self.vectorContextMenu.exec_( self.treeWidgetVector.mapToGlobal(point))

    ################# STRDS CONTEXT MENUES ##############################

    def call_strds_context_menu(self):
        item = self.treeWidgetStrds.currentItem()
        # These two functions  will select the correct type (raster or STRDS)
        self.strds_name_selected(item, 0)
        self.strds_raster_name_selected(item, 0)
        self.show_strds_raster_info(item, 0)
        self.show_strds_info(item, 0)
        self.load_grass_strds_raster_layer()

    def on_strds_context_menu(self, point):
         self.strdsContextMenu.exec_( self.treeWidgetStrds.mapToGlobal(point))

    ################# STVDS CONTEXT MENUES ##############################

    def call_stvds_context_menu(self):
        item = self.treeWidgetStvds.currentItem()
        # These two functions will select the correct type (vector or STVDS)
        self.stvds_name_selected(item, 0)
        self.stvds_vector_name_selected(item, 0)
        self.show_stvds_vector_info(item, 0)
        self.show_stvds_info(item, 0)
        self.load_grass_stvds_vector_layer()

    def on_stvds_context_menu(self, point):
         self.stvdsContextMenu.exec_( self.treeWidgetStvds.mapToGlobal(point))

    ####################################################################
    ################# SELECT NAMES IN LINE EDIT ########################
    ####################################################################

    def vector_name_selected(self, item, column):
        """
        Put a vector layer name into the vector load line edit
        :return: True if the vector name was set as text, otherwise False
        """

        if not item:
            return False

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return False
        self.vectorLayerLine.setText(item.text(0) + "@" + item.text(1))
        return True

    def raster_name_selected(self, item, column):
        """
        Put a raster layer name into the raster load line edit
        :return: True if the vector name was set as text, otherwise False
        """

        if not item:
            return False

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return False
        self.rasterLayerLine.setText(item.text(0) + "@" + item.text(1))
        return True

    def strds_raster_name_selected(self, item, column):
        """
        Put a raster layer name into the raster load line edit
        in the STRDS dialog.
        :return: True if the vector name was set as text, otherwise False
        """

        if not item:
            return False

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return False
        self.strdsRasterLayerLine.setText(item.text(0) + "@" + item.text(1))
        return True

    def strds_name_selected(self, item, column):
        """
        Put a strds name into the strds/raster load line edit
        in the STRDS dialog.
        :return: True if the vector name was set as text, otherwise False
        """

        if not item:
            return False

        if item.text(1) != "is_stds!":
            return False
        self.strdsRasterLayerLine.setText("STRDS::" + item.text(0))
        return True

    def stvds_vector_name_selected(self, item, column):
        """
        Put a vector layer name into the vector load line edit
        in the STVDS dialog.
        :return: True if the vector name was set as text, otherwise False
        """

        if not item:
            return False

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return False
        self.stvdsVectorLayerLine.setText(item.text(0) + "@" + item.text(1))
        return True

    def stvds_name_selected(self, item, column):
        """
        Put a stvds name into the strds/raster load line edit
        in the STVDS dialog.
        :return: True if the vector name was set as text, otherwise False
        """

        if not item:
            return False

        if item.text(1) != "is_stds!":
            return False
        self.stvdsVectorLayerLine.setText("STVDS::" + item.text(0))
        return True

    ####################################################################
    ################# SHOW INFO ABOUT LAYERS ###########################
    ####################################################################

    def show_strds_info(self, item, column):
        """
        Show information about a STRDS in a plain text widgets of the
        STRDS dialog. Show in additon the first raster map layer in
        the preview label.
        """

        if not item:
            return False

        if item.text(1) != "is_stds!":
            return

        strds_name = item.text(0)

        if strds_name in self.strds:
            text = self._generate_stds_info(strds_name, "strds")
            self.plainTextEditStrds.setPlainText(text)

            raster_name = self.strds[strds_name]["first_map"]
            # STRDS is empty
            if raster_name is None:
                return
            mapset = self.strds[strds_name]["mapset"]

            # We are only interested in the image
            geom = self.strdsImageLabel.size()
            text, image = self._generate_raster_info(raster_name,
                                                     mapset,
                                                     geom)
            if text is None or image is None:
                return
            self.strds_image = image
            self.strds_raster_pixmap = QtGui.QPixmap.fromImage(self.strds_image)
            self.strdsImageLabel.setPixmap(self.strds_raster_pixmap)
        else:
            return

    def show_stvds_info(self, item, column):
        """
        Show information about a STVDS in a plain text widgets of the
        STVDS dialog
        """

        if not item:
            return False

        if item.text(1) != "is_stds!":
            return

        text = self._generate_stds_info(item.text(0), "stvds")
        self.plainTextEditStvds.setPlainText(text)

    def _generate_stds_info(self, name, stds_type):
        """
        Show information about a space time dataset in a plain text widget
        """
        text = ""
        try:
            self.dbif.connect()
            stds = tgis.open_old_stds(name, stds_type, self.dbif)
            stds.select(self.dbif)

            with Capturing() as output:
                #stds.print_shell_info()
                print("--- Basic information ---")
                stds.base.print_shell_info()
                print("--- Temporal information ---")
                stds.temporal_extent.print_shell_info()
                print("--- Spatial information ---")
                stds.spatial_extent.print_shell_info()
                print("--- Metadata information ---")
                stds.metadata.print_shell_info()

            format_text = []

            for line in output:
                if "=" in line:
                    key, value = line.split("=", 1)
                    string = key.ljust(20, ".") + ":  " + value
                    format_text.append(string)
                else:
                    format_text.append("++--++" + line + "++--++")

            text = "\n".join(format_text)
            text = text.replace("++--++", "\n")

            # Additional metadata
            text += "\n"
            text += "Title".ljust(20, ".") + ":  " +\
                    str(stds.metadata.get_title())
            text += "\n"
            text += "Description".ljust(20, ".") + ":  " +\
                    str(stds.metadata.get_description())
            text += "\n"
            text += "\n"
            text += "--- Command history ---"
            text += "\n"
            text += "\n"
            text += stds.metadata.get_command()

            if stds_type == "stvds":
                # Set the feature type selection based on the stvds info
                if stds.metadata.get_number_of_areas() > 0:
                    self.stvdsVectorTypeChooser.setCurrentIndex(4)
                elif stds.metadata.get_number_of_boundaries() > 0:
                    self.stvdsVectorTypeChooser.setCurrentIndex(3)
                elif stds.metadata.get_number_of_lines() > 0:
                    self.stvdsVectorTypeChooser.setCurrentIndex(2)
                elif stds.metadata.get_number_of_points() > 0:
                    self.stvdsVectorTypeChooser.setCurrentIndex(0)
        except:
            QtGui.QMessageBox.critical(self, "GRASS Fatal Error", "Unable to find GRASS space time dataset: %s"%name)
        finally:
            self.dbif.close()
            return text

    def show_strds_raster_info(self, item, column):
        """Show information about a raster map layer in the  plain
           text widget and the label widget of the STRDS dialog
        """

        if not item:
            return

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return

        geom = self.strdsImageLabel.size()
        text, image = self._generate_raster_info(item.text(0),
                                                 item.text(1),
                                                 geom)

        if text is None or image is None:
            return

        self.strds_image = image

        self.strds_raster_pixmap = QtGui.QPixmap.fromImage(self.strds_image)

        self.plainTextEditStrds.setPlainText(text)
        self.strdsImageLabel.setPixmap(self.strds_raster_pixmap)

    def show_raster_info(self, item, column):
        """Show information about a raster map layer in the  plain
           text widget and the label widget of the raster dialog
        """

        if not item:
            return

        if item.text(1) == "is_mapset!":
            return

        geom = self.rasterImageLabel.size()
        text, image = self._generate_raster_info(item.text(0),
                                                 item.text(1),
                                                 geom)

        if text is None or image is None:
            return

        self.raster_image = image
        self.raster_pixmap = QtGui.QPixmap.fromImage(self.raster_image)

        self.plainTextEditRaster.setPlainText(text)
        self.rasterImageLabel.setPixmap(self.raster_pixmap)

    def _generate_raster_info(self, name, mapset, geom):
        """
        Generate formatted output and preview image for raster map
        :param name: Name of the raster map
        :param mapset: Name of the mapsets of the raster map
        :param geom: QSize object defining the size of the preview image
        :return:
        """

        try:
            info = self.grass_iface.read_raster_full_info(name, mapset)
        except Exception, e:
            QtGui.QMessageBox.critical(self, "GRASS Fatal Error", "Fatal error occurred in GRASS: %s"%str(e))
            return None, None

        if not info:
            QtGui.QMessageBox.critical(self, "GRASS Fatal Error", "Unable to find GRASS raster layer: %s"%name)
            return None, None

        content = []

        content = []
        content.append("--- Metadata ---")
        content.append(" ")
        content.append("  Full name...:  %s" % (info["full_name"]))
        content.append("  Mapset......:  %s" % (info["mapset"]))
        content.append("  Title.......:  %s" % (info["title"]))
        content.append("  Creator.....:  %s" % (info["creator"]))
        content.append("  Comment.....:  %s" % (info["keyword"]))
        if "start_time" in info:
            content.append("  Start time..:  %s" % (info["start_time"]))
            if info["end_time"] is not None:
                content.append("  End time....:  %s" % (info["end_time"]))
            if "unit" in info:
                content.append("  Time unit...:  %s" % (info["unit"]))

        content.append("  Rows........:  %i" % (info["rows"]))
        content.append("  Cols........:  %i" % (info["cols"]))
        content.append("  Cells.......:  %i" % (int(info["rows"]) *
                                                int(info["cols"])))
        content.append("  Min.........:  %f" % (info["min"]))
        content.append("  Max.........:  %f" % (info["max"]))
        content.append("  Type........:  %s" % (info["mtype"]))
        content.append("  Map date....:  %s" % (info["date"]))
        content.append("  Projection..:  %i" % (info["proj"]))
        content.append("  Zone........:  %i" % (info["zone"]))
        content.append("  Source 1....:  %s" % (info["src1"]))
        content.append("  Source 2....:  %s" % (info["src2"]))
        content.append(" ")
        content.append("--- Extent ---")
        content.append(" ")
        content.append("  north.......:  %f" % (info["north"]))
        content.append("  south.......:  %f" % (info["south"]))
        content.append("  east........:  %f" % (info["east"]))
        content.append("  west........:  %f" % (info["west"]))
        content.append("  ewres.......:  %f" % (info["ewres"]))
        content.append("  nsres.......:  %f" % (info["nsres"]))

        # Create ASCII cats table with nice formatting
        if "cats" in info:
            content.append(" ")
            content.append("--- Categories ---")
            content.append(" ")
            content.append("  %s" % (info["cats_title"]))
            content.append(" ")

            max_len = 0
            for entry in info["cats"]:
                if len(entry) > 0:
                    if len(entry[0]) > max_len:
                        max_len = len(entry[0])

            first = "category".ljust(max_len + 2)
            content.append("  %s|     raster values" % (first))
            content.append("  " + "-".ljust(max_len + 2 + 19, "-"))

            cats = info["cats"]
            for entry in cats:
                if len(entry) > 0:
                    first = entry[0].ljust(max_len + 2)
                    if len(entry) == 3:
                        if entry[2] is not None:
                            content.append("  %s|     %s  -  %s " % (first,
                                                             str(entry[1]),
                                                             str(entry[2])))
                        else:
                            content.append("  %s|     %s" % (first,
                                                           str(entry[1])))
                    elif len(entry) == 2:
                        content.append("  %s|     %s" % (first,
                                                       str(entry[1])))
                    else:
                        content.append("  %s|     %s" % (first,
                                                       str(entry[1:])))

        cols = geom.width() - 2
        rows = geom.height() - 2

        extent = {"rows": rows, "cols": cols}

        array = self.grass_data_provider.get_raster_image_as_np(name,
                                                                mapset,
                                                                extent,
                                                                color="RGB")
        image = QtGui.QImage(array.data, cols, rows,
                             QtGui.QImage.Format_RGB32)

        return(("\n".join(content), image))

    def show_stvds_vector_info(self, item, column):
        """Show information about a vector map layer in the  plain
           text widget and the label widget of the STVDS dialog
        """

        if not item:
            return

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return

        text = self._generate_vector_info(item.text(0), item.text(1),
                                          self.stvdsVectorTypeChooser)

        if text:
            self.plainTextEditStvds.setPlainText(text)

    def show_vector_info(self, item, column):
        """Show information about a vector map layer in the  plain
           text widget and the label widget of the vector dialog
        """

        if not item:
            return

        if item.text(1) == "is_mapset!" or item.text(1) == "is_stds!":
            return

        text = self._generate_vector_info(item.text(0), item.text(1),
                                          self.vectorTypeChooser)

        if text:
            self.plainTextEditVector.setPlainText(text)

    def _generate_vector_info(self, vector_name, mapset, type_chooser):

        try:
            info = self.grass_iface.read_vector_full_info(vector_name, mapset)
        except Exception, e:
            QtGui.QMessageBox.critical(self, "GRASS Fatal Error", "Fatal error occurred in GRASS: %s"%str(e))
            return None

        if not info:
            QtGui.QMessageBox.critical(self, "GRASS Fatal Error", "Unable to find GRASS vector layer: %s"%vector_name)
            return None

        # Set the feature type selection based on the vector info
        if info["areas"] > 0:
            type_chooser.setCurrentIndex(4)
        elif info["boundary"] > 0:
            type_chooser.setCurrentIndex(3)
        elif info["line"] > 0:
            type_chooser.setCurrentIndex(2)
        elif info["point"] > 0:
            type_chooser.setCurrentIndex(0)

        content = []
        content.append("--- Metadata ---")
        content.append(" ")
        content.append("  Full name.....:  %s" % (info["full_name"]))
        content.append("  Mapset........:  %s" % (info["mapset"]))
        content.append("  Title.........:  %s" % (info["title"]))
        content.append("  Organization..:  %s" % (info["organization"]))
        content.append("  Creator.......:  %s" % (info["person"]))
        content.append("  Map date......:  %s" % (str(info["map_date"])))
        content.append("  Comment.......:  %s" % (info["comment"]))
        if "start_time" in info:
            content.append("  Start time..:  %s" % (info["start_time"]))
            if info["end_time"] is not None:
                content.append("  End time....:  %s" % (info["end_time"]))
            if "unit" in info:
                content.append("  Time unit...:  %s" % (info["unit"]))
        content.append("  Is3d..........:  %i" % (bool(info["is_3d"])))
        content.append("  dbase links...:  %i" % (info["dblinks"]))
        content.append("  dig. thresh...:  %f" % (info["thresh"]))
        content.append("  Projection....:  %s" % (info["proj"]))
        content.append("  Proj. name....:  %s" % (info["proj_name"]))
        content.append("  Zone..........:  %s" % (info["zone"]))
        content.append(" ")
        content.append("--- Features ---")
        content.append(" ")
        content.append("  Areas.........:  %i" % (info["areas"]))
        content.append("  Boundaries....:  %i" % (info["boundary"]))
        content.append("  Centroids.....:  %i" % (info["centroid"]))
        content.append("  Lines.........:  %i" % (info["line"]))
        content.append("  Nodes.........:  %i" % (info["nodes"]))
        content.append("  Points........:  %i" % (info["point"]))
        content.append("  Holes.........:  %i" % (info["holes"]))
        content.append("  Islands.......:  %i " % (info["islands"]))
        content.append("  Faces.........:  %i" % (info["face"]))
        content.append("  Kernels.......:  %i" % (info["kernel"]))
        content.append("  Volumes.......:  %i" % (info["volume"]))
        content.append("  Updated lines.:  %i" % (info["updated_lines"]))
        content.append("  Updated nodes.:  %i" % (info["updated_nodes"]))
        # Not sure what this is
        #content.append("  Vector lines..:  %i" % (info["lines"]))
        content.append("  Vertices......:  %i" % (info["points"]))
        content.append(" ")
        content.append("--- Extent ---")
        content.append(" ")
        content.append("  North.........:  %f" % (info["north"]))
        content.append("  South.........:  %f" % (info["south"]))
        content.append("  East..........:  %f" % (info["east"]))
        content.append("  West..........:  %f" % (info["west"]))
        content.append("  Top...........:  %f" % (info["top"]))
        content.append("  Bottom........:  %f" % (info["bottom"]))

        # Create ASCII columns table with nice formatting
        if "columns" in info:

            max_len = 0
            for name in info["columns"]:
                if len(name) > max_len:
                    max_len = len(name)

            content.append(" ")
            content.append("--- Database columns ---")
            content.append(" ")

            first = "name".ljust(max_len + 2)
            content.append("  %s|     data type" % (first))
            content.append("  " + "-".ljust(max_len + 2 + 15, "-"))

            columns = info["columns"]
            for name in columns:
                first = name.ljust(max_len + 2)
                content.append("  %s|     %s" % (first, columns[name]))

        return("\n".join(content))

    ####################################################################
    ################# LIST MAPS AND STDS ###############################
    ####################################################################

    def list_raster_maps(self):
        """List pattern specific raster maps in the raster tree widget
        :return:
        """
        self.rasterImageLabel.clear()
        self.list_maps(self.treeWidgetRaster,
                       self.plainTextEditRaster,
                       self.lineEditRasterPattern,
                       "raster")

    def list_vector_maps(self):
        """List pattern specific vector maps in the raster tree widget
        :return:
        """
        self.list_maps(self.treeWidgetVector,
                       self.plainTextEditVector,
                       self.lineEditVectorPattern,
                       "vector")

    def list_maps(self,
                  treeWidget,
                  plainTextEdit,
                  lineEditPattern,
                  type_):
        """
        Fill the tree widgets with raster
        and vector map names, mapset specific
        :param treeWidget: The treeWidget to be filled with data
        :param plainTextEdit: The map layer info text planetextEdit
        :param lineEditPattern: The g.list pattern to select a map layer subset
        :param type_: The map layer type (raster, vector)
        :return:
        """
        treeWidget.clear()
        plainTextEdit.clear()
        treeWidget.setColumnCount(2)
        treeWidget.setColumnHidden(1, True)
        treeWidget.setHeaderLabels(["Map names", "Hidden"])

        for mapset in self.accessable_mapsets:

            rmapset_item = QtGui.QTreeWidgetItem()

            pattern = lineEditPattern.text()
            if not pattern or pattern == "":
                pattern = "*"

            count = 0
            # Read maps for this mapset
            maps = gscript.read_command("g.list",
                                        type=type_,
                                        mapset=mapset,
                                        pattern=pattern)
            for map_name in maps.split():
                map_item = QtGui.QTreeWidgetItem()
                map_item.setText(0, map_name)
                map_item.setText(1, mapset)
                rmapset_item.addChild(map_item)
                count += 1
            rmapset_item.setText(0, mapset + "    (%i maps)" % count)
            rmapset_item.setText(1, "is_mapset!")

            treeWidget.insertTopLevelItem(0, rmapset_item)
            rmapset_item.setExpanded(True)

    def list_strds_maps(self):
        """
        List STRDS and their map layers for each mapset in a treeWidget
        :return:
        """
        self.list_stds_maps(self.treeWidgetStrds,
                            self.strdsImageLabel,
                            self.plainTextEditStrds,
                            "strds",
                            self.lineEditSTRDSWhere.text(),
                            True)

    def list_stvds_maps(self):
        """
        List STVDS and their map layers for each mapset in a treeWidget
        :return:
        """
        self.list_stds_maps(self.treeWidgetStvds,
                            None,
                            self.plainTextEditStvds,
                            "stvds",
                            self.lineEditSTVDSWhere.text(),
                            True)

    def _create_time_string(self, start, end):
        """
        Create the time strings out of datetime objects
        containing date or date and time

        :param start: start time datetime object
        :param end: end time datetime object
        :return: start, end time strings
        """

        if start is None:
            return "None", "None"

        if end is None:
            end = start

        d, t = str(start).split()
        if start.hour == 0 and start.minute == 0 and start.second == 0:
            start_string = "%s"%d
        else:
            start_string = "%s %s"%(d, t)

        d, t = str(end).split()
        if start.hour == 0 and start.minute == 0 and start.second == 0:
            end_string = "%s"%d
        else:
            end_string = "%s %s"%(d, t)

        return start_string, end_string

    def list_stds_maps(self, treeWidget,
                       ImageLabel=None,
                       plainTextEdit=None,
                       stds_type="strds",
                       where="",
                       list_maps=True):
        """
        Fill the tree widgets with STDS and their raster
        and vector map names, mapset specific

        :param treeWidget: The tree widget to be filled with data
        :param ImageLabel: The label in which raster map preview pictures are displayed
        :param plainTextEdit: The plainTextEdit that displays the info
        :param stds_type: The STDS type (strds, stvds)
        :param where:  The where statement to select specific STDS
        :param list_maps: Set True to list all maps in a STDS
        :return: None
        """

        self.dbif.connect()
        try:
            treeWidget.clear()

            if ImageLabel is not None:
                ImageLabel.clear()
            if plainTextEdit is not None:
                plainTextEdit.clear()

            treeWidget.setColumnCount(6)
            treeWidget.setColumnHidden(1, True)
            treeWidget.setColumnHidden(5, True)
            treeWidget.setHeaderLabels(["Dataset names", "is_stds",
                                        "Start time", "End time",
                                        "Maps", "stds_type"])
            try:
                stds_list_abs = tgis.get_dataset_list(stds_type, "absolute",
                                                      where=where,
                                                      columns="id,start_time,"
                                                              "end_time")
                stds_list_rel = tgis.get_dataset_list(stds_type, "relative",
                                                      where=where,
                                                      columns="id,start_time,"
                                                              "end_time")
            except Exception, e:
                QtGui.QMessageBox.critical(self, "Error", "Error in SQL:\n%s"%(str(e)))
                return

            stds_list = []

            if stds_list_rel and stds_list_abs:
                stds_list = stds_list_abs.update(stds_list_rel)
            elif stds_list_rel:
                stds_list = stds_list_rel
            elif stds_list_abs:
                stds_list = stds_list_abs

            for mapset in self.accessable_mapsets:

                rmapset_item = QtGui.QTreeWidgetItem()
                rmapset_item.setText(0, mapset)
                rmapset_item.setText(1, "is_mapset!")

                if mapset in stds_list:
                    rmapset_item.setText(0, mapset + "    (%i %s)" %
                                    (len(stds_list[mapset]), stds_type))
                    for stds in stds_list[mapset]:
                        stds_name, start, end = stds

                        strds_item = QtGui.QTreeWidgetItem()
                        strds_item.setText(0, stds_name)
                        strds_item.setText(1, "is_stds!")

                        start_str, end_str = self._create_time_string(start,
                                                                      end)
                        strds_item.setText(2, start_str)
                        strds_item.setText(3, end_str)

                        stds = tgis.open_old_stds(stds_name,
                                                  stds_type,
                                                  self.dbif)
                        stds.select(self.dbif)

                        number_of_maps = stds.metadata.get_number_of_maps()

                        if number_of_maps is None:
                            number_of_maps = 0

                        strds_item.setText(4, "%i" % (number_of_maps))

                        strds_item.setText(5, stds_type)
                        rmapset_item.addChild(strds_item)
                        maps = stds.get_registered_maps(columns="name,start_"
                                                                "time,end_time",
                                                        order="start_time",
                                                        dbif=self.dbif)
                        # We need to store some informations about the STDS
                        if len(maps) > 0:
                            map_name, dummy, dummy = maps[0]
                            if stds_type == "strds":
                                self.strds[stds_name] = {"first_map": map_name,
                                                         "mapset": mapset}
                            else:
                                self.stvds[stds_name] = {"first_map": map_name,
                                                         "mapset": mapset}

                            for _map in maps:
                                name, start, end = _map

                                if end is None:
                                    end = start

                                map_item = QtGui.QTreeWidgetItem()
                                map_item.setText(0, name)
                                map_item.setText(1, mapset)
                                start_str, end_str = self._create_time_string(start,
                                                                              end)
                                map_item.setText(2, start_str)
                                if start != end:
                                    map_item.setText(3, end_str)
                                if list_maps is True:
                                    strds_item.addChild(map_item)

                treeWidget.insertTopLevelItem(0, rmapset_item)
                rmapset_item.setExpanded(True)
        except:
            raise
        finally:
            self.dbif.close()
            treeWidget.resizeColumnToContents(0)
            treeWidget.resizeColumnToContents(1)
            treeWidget.resizeColumnToContents(2)
            treeWidget.resizeColumnToContents(3)

    ####################################################################
    ################# FILL MAIN TEXT INFO WIDGET WITH CONTENT #########
    ####################################################################

    def read_info_fill_widget(self):
        """
        Read all location and mapset specific
        information and fill the information-tree-widget
        with the collected content
        :return:
        """

        position = 0

        # Init the info tree widget
        self.treeWidgetInfo.clear()
        self.treeWidgetInfo.setColumnCount(1)

        # Run the GRASS commands to gather informations
        env = gscript.gisenv()
        region_settings =  gscript.read_command("g.region", flags="p3")
        proj_string = gscript.read_command("g.proj", flags="p")
        datum_string = gscript.read_command("g.proj", flags="d")
        wkt_proj_string = gscript.read_command("g.proj", flags="w")
        build_string = gscript.read_command("g.version", flags="bre")

        # Location
        widget = QtGui.QTreeWidgetItem()
        widget.setText(0, "Location Information (g.gisenv)")

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "%s: %s"%("GRASS database path".ljust(30, "."), env["GISDBASE"]))
        widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "%s: %s"%("Location name".ljust(30, "."), env["LOCATION_NAME"]))
        widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "%s: %s"%("Mapset".ljust(30, "."), env["MAPSET"]))
        widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "%s: %s"%("Graphical User Interface".ljust(30, "."), env["GUI"]))
        widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "%s: %s"%("Process ID".ljust(30, "."), env["PID"]))
        widget.addChild(content)

        # Mapsets
        content = QtGui.QTreeWidgetItem()
        content.setText(0, "Mapsets in search path (g.mapsets -p)")
        for mapset in self.accessable_mapsets:
            mapset_widget = QtGui.QTreeWidgetItem()
            mapset_widget.setText(0, mapset)
            content.addChild(mapset_widget)
        widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "Available mapsets (g.mapset -l)")
        for mapset in self.mapsets:
            mapset_widget = QtGui.QTreeWidgetItem()
            mapset_widget.setText(0, mapset)
            content.addChild(mapset_widget)
        widget.addChild(content)

        self.treeWidgetInfo.insertTopLevelItem(position, widget)

        # Region
        position += 1
        widget = QtGui.QTreeWidgetItem()
        widget.setText(0, "Region settings (g.region -p3)")

        for line in region_settings.split("\n"):
            if ":" in line:
                content = QtGui.QTreeWidgetItem()
                key, value = line.split(":", 1)
                line = "%s: %s"%(key.strip().ljust(30, "."), value.strip())

                content.setText(0, line)
                widget.addChild(content)

        self.treeWidgetInfo.insertTopLevelItem(position, widget)

        # Projection info
        position += 1
        widget = QtGui.QTreeWidgetItem()
        widget.setText(0, "Projection Information (g.proj -pdw)")

        for line in proj_string.split("\n"):
            if ":" in line:
                content = QtGui.QTreeWidgetItem()
                key, value = line.split(":", 1)
                line = "%s: %s"%(key.strip().ljust(30, "."), value.strip())

                content.setText(0, line)
                widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "Datum Information")

        datum_widget = QtGui.QTreeWidgetItem()
        datum_widget.setText(0, datum_string)

        content.addChild(datum_widget)
        widget.addChild(content)

        content = QtGui.QTreeWidgetItem()
        content.setText(0, "Well Known Text (WKT) Representation")

        wkt_widget = QtGui.QTreeWidgetItem()
        wkt_widget.setText(0, wkt_proj_string)

        content.addChild(wkt_widget)
        widget.addChild(content)

        self.treeWidgetInfo.insertTopLevelItem(position, widget)

        # Environment
        position += 1
        widget = QtGui.QTreeWidgetItem()
        widget.setText(0, "Shell Environment")

        for entry in os.environ:
            if "GRASS" in entry:
                content = QtGui.QTreeWidgetItem()
                content.setText(0, "%s: %s"%(entry.ljust(30, "."), os.environ[entry]))
                widget.addChild(content)

        self.treeWidgetInfo.insertTopLevelItem(position, widget)

        # Build
        position += 1
        widget = QtGui.QTreeWidgetItem()
        widget.setText(0, "Build Information (g.version -bre)")

        for build_info in build_string.split("\n"):
            content = QtGui.QTreeWidgetItem()
            content.setText(0, build_info.strip())
            widget.addChild(content)
        widget.setExpanded(True)

        self.treeWidgetInfo.insertTopLevelItem(position, widget)

        self.treeWidgetInfo.expandAll()

    ####################################################################
    ################# GRASS MODULE TREE GENERATION #####################
    ####################################################################

    def create_module_tree(self):
        """
        Generate the GRASS module tree based on the GRASS GUI module_items.xml file.
        Search phrases can be used to search in module names, keywords and description.
        :return:
        """

        search_phrase = self.lineEditSearchModule.text()
        search_phrases = search_phrase.split()

        # Load default page
        url = os.path.join(self.gisbase, "docs", "html", "index.html")
        self.moduleHTMLHelp.load(QtCore.QUrl(url))
        self.moduleHTMLHelp.show()

        self.treeWidgetGRASSModules.clear()
        self.treeWidgetGRASSModules.setColumnCount(1)

        level_one_position = 0
        for first_key in self.modules_keyword_dict:

            level_one_widget = QtGui.QTreeWidgetItem()
            level_one_widget.setText(0, first_key)

            level_one = self.modules_keyword_dict[first_key]

            level_two_position = 0
            for second_key in level_one:

                level_two_widget = QtGui.QTreeWidgetItem()
                level_two_widget.setText(0, second_key)

                level_two = level_one[second_key]

                level_three_position = 0
                for module in level_two:
                    entry = level_two[module]

                    keywords = entry["keywords"]
                    description = entry["description"]

                    # Search for phrase in module name, keywords and description
                    if len(search_phrase) > 0 and search_phrase != "*":

                        matched_phrases = []
                        for phrase in search_phrases:
                            found_match = False
                            if module.find(phrase) >= 0:
                                found_match = True
                            if self.checkBoxCheckKeywords.isChecked() is True:
                                for keyword in keywords.split(","):
                                    if keyword.find(phrase) >= 0:
                                        found_match = True
                            if self.checkBoxCheckDescription.isChecked() is True:
                                if description.find(phrase) >= 0:
                                    found_match = True
                            if found_match is True:
                                matched_phrases.append(phrase)

                        # Check if all phrases where matched
                        skip_module = False
                        for phrase in search_phrases:
                            if phrase not in matched_phrases:
                                skip_module = True
                                break

                        # Skip the module if a single search phrase is missing
                        if skip_module is True:
                            continue

                    level_three_widget = QtGui.QTreeWidgetItem()
                    level_three_widget.setText(0, module)
                    level_three_widget.setData(0, QtCore.Qt.UserRole, module)
                    level_two_widget.insertChild(level_three_position, level_three_widget)
                    level_three_position += 1

                if level_three_position > 0:
                    level_one_widget.insertChild(level_two_position, level_two_widget)
                    level_two_widget.setExpanded(True)
                    level_two_position += 1

            if level_two_position > 0:
                self.treeWidgetGRASSModules.insertTopLevelItem(level_one_position,
                                                               level_one_widget)
                level_one_position += 1

        # Expand all if a search pattern was provided
        if len(search_phrase) > 0 and search_phrase != "*":
            self.treeWidgetGRASSModules.expandAll()
        else:
            # Expand level two entries
            for i in range(self.treeWidgetGRASSModules.topLevelItemCount()):
                first_level_item = self.treeWidgetGRASSModules.topLevelItem(i)
                if first_level_item is not None:
                    for j in range(first_level_item.childCount()):
                        second_level_item = first_level_item.child(j)
                        second_level_item.setExpanded(True)

    def module_name_selected(self, item, column):
        """
        Set the manual page in the module help widget based on the
        module name of the selected widget in the module tree
        """
        module_name = item.data(0, QtCore.Qt.UserRole)

        if not module_name:
            return False

        url = os.path.join(self.gisbase, "docs", "html", module_name + ".html")
        self.moduleHTMLHelp.load(QtCore.QUrl(url))
        self.moduleHTMLHelp.show()

    def module_name_selected_by_doubleclick(self, item, column):
        """
        Set the manual page in the module help widget based on the
        module name of the selected widget in the module tree
        """
        module_name = item.data(0, QtCore.Qt.UserRole)

        if not module_name:
            return False

        args = [os.environ["GRASS_PYTHON"], self.forms_generation_path, module_name]
        p = subprocess.Popen(args=args, shell=False)

    ####################################################################
    ################# GRASS REGION SETTINGS ############################
    ####################################################################

    def load_current_region_to_widget(self):

        region = gscript.region()
        self.lineEditNorth.setText(str(region["n"]))
        self.lineEditSouth.setText(str(region["s"]))
        self.lineEditEast.setText(str(region["e"]))
        self.lineEditWest.setText(str(region["w"]))
        self.lineEditCols.setText(str(region["cols"]))
        self.lineEditRows.setText(str(region["rows"]))
        self.lineEditNSResolution.setText(str(region["nsres"]))
        self.lineEditEWResolution.setText(str(region["ewres"]))

    def load_region_from_canvas_to_widget(self):

        region = gscript.region()
        map_settings = self.canvas.mapSettings()
        rect = self.canvas.extent()
        self._reproject_rect_to_grass_crs(map_settings.destinationCrs(), rect)

        east = rect.xMaximum()
        west = rect.xMinimum()
        north = rect.yMaximum()
        south = rect.yMinimum()

        self.lineEditNorth.setText(str(north))
        self.lineEditSouth.setText(str(south))
        self.lineEditEast.setText(str(east))
        self.lineEditWest.setText(str(west))

        self.lineEditCols.setText(str(region["cols"]))
        self.lineEditRows.setText(str(region["rows"]))
        self.lineEditNSResolution.setText(str(region["nsres"]))
        self.lineEditEWResolution.setText(str(region["ewres"]))

    def load_region_from_active_layer_to_widget(self):

        region = gscript.region()
        layer = self.iface.activeLayer()

        if layer:
            crs = layer.crs()
            rect = layer.extent()

            self._reproject_rect_to_grass_crs(crs, rect)

            east = rect.xMaximum()
            west = rect.xMinimum()
            north = rect.yMaximum()
            south = rect.yMinimum()

            self.lineEditNorth.setText(str(north))
            self.lineEditSouth.setText(str(south))
            self.lineEditEast.setText(str(east))
            self.lineEditWest.setText(str(west))

            self.lineEditCols.setText(str(region["cols"]))
            self.lineEditRows.setText(str(region["rows"]))
            self.lineEditNSResolution.setText(str(region["nsres"]))
            self.lineEditEWResolution.setText(str(region["ewres"]))

    def set_region_from_widget(self):

        north = self.lineEditNorth.text()
        south = self.lineEditSouth.text()
        east = self.lineEditEast.text()
        west = self.lineEditWest.text()
        cols = self.lineEditCols.text()
        rows = self.lineEditRows.text()
        nsres = self.lineEditNSResolution.text()
        ewres = self.lineEditEWResolution.text()

        ignore_res = self.checkBoxIgnoreResolution.isChecked()
        ignore_rc = self.checkBoxIgnoreRowsAndCols.isChecked()

        if ignore_rc is True and ignore_res is True:
            gscript.run_command("g.region", n=north,
                                s=south, e=east,
                                w=west, flags="p")
        elif ignore_rc is True:
            gscript.run_command("g.region", n=north,
                                s=south, e=east,
                                w=west, nsres=nsres,
                                ewres=ewres, flags="p")
        elif ignore_res is True:
            gscript.run_command("g.region", n=north,
                                s=south, e=east,
                                w=west, cols=cols,
                                rows=rows, flags="p")
        else:
            gscript.run_command("g.region", n=north,
                                s=south, e=east,
                                w=west, nsres=nsres,
                                ewres=ewres, cols=cols,
                                rows=rows, flags="p")

        self.load_current_region_to_widget()
        self.show_update_region(update_only=True)
        self.read_info_fill_widget()

    def _reproject_rect_to_grass_crs(self, source_crs, rect):
        """
        Reproject a rectangle to the GRASS coordinate reference system
        :param source_crs:
        :param rect:
        :return:
        """

        transformer = QgsCoordinateTransform()
        transformer.setSourceCrs(source_crs)
        wkt_proj = gscript.read_command("g.proj", flags="w")
        transformer.setDestCRS(QgsCoordinateReferenceSystem(wkt_proj))
        transformer.transformBoundingBox(rect)












