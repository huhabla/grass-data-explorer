# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GrassModuleReader
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import *
from PyQt4.QtXml import *
import os

class GrassModuleReader(object):
    """
    Class to parse the module_items.xml file from GRASS

    This class creates a nested module dictionary based on keywords.
    The keyword depth is 2, if the second keyword is missing, the
    keyword "unspecific" is used as default.
    """

    def __init__(self, filename):

        self.modules_keyword_dict = {}

        file = QFile(filename)
        file.open(QIODevice.ReadOnly)
        self.xml = QXmlStreamReader(file)

        while not self.xml.atEnd():
            self.xml.readNext()
            if self.xml.isStartElement():
                if self.xml.name() == "module-item":
                    name = self.xml.attributes().value("name")
                    if name:
                        self.parse_module()

    def parse_module(self):

        module = None
        keywords = None
        description = None

        while self.xml.readNextStartElement():

            if self.xml.name() == "module":
                module = self.xml.readElementText()
            elif self.xml.name() == "description":
                description = self.xml.readElementText()
            elif self.xml.name() == "keywords":
                keywords = self.xml.readElementText()

        if module and keywords and description:
            # First and second keyword must exist
            key_list = str(keywords).split(",")
            first_key = key_list[0]
            second_key = "unspecific"
            if len(key_list) > 1:
                second_key = key_list[1]

            if first_key not in self.modules_keyword_dict:
                self.modules_keyword_dict[first_key] = {}
            if second_key not in self.modules_keyword_dict[first_key]:
                self.modules_keyword_dict[first_key][second_key] = {}

            module_entry = self.modules_keyword_dict[first_key][second_key]
            module_entry[str(module)] = {}
            module_entry[str(module)]["module"] = str(module)
            module_entry[str(module)]["description"] = str(description)
            module_entry[str(module)]["keywords"] = str(keywords)

if __name__ == "__main__":
    gisbase = os.environ["GISBASE"]

    url = os.path.join(gisbase, "gui", "wxpython", "xml", "module_items.xml")
    xml = GrassModuleReader(url)

    for first_key in xml.modules_keyword_dict:
        print(first_key)
        level_one = xml.modules_keyword_dict[first_key]
        for second_key in level_one:
            print("  " + second_key)
            level_two = level_one[second_key]
            for module in level_two:
                entry = level_two[module]
                print("    " + entry["module"])
                print("      " + entry["description"])

