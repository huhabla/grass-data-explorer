# -*- coding: utf-8 -*-
"""
/***************************************************************************
 grass_region_to_memory_layer
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import qgis.core as core
import grass.script as gscript


def update_memory_layer_by_region(layer, grass_region, emit=True):
    """
    Update the memory layer with region settings
    and emit data change and repaint request.

    :param layer: The memory layer to modify
    :param grass_region: The GRASS region dict to convert
    :param emit: If True emit data changed and redraw request signals
    :return:
    """

    ids = layer.allFeatureIds()
    data_provider = layer.dataProvider()
    data_provider.deleteFeatures(ids)

    line = [core.QgsPoint(grass_region["w"], grass_region["s"]),
                core.QgsPoint(grass_region["w"], grass_region["n"]),
                core.QgsPoint(grass_region["e"], grass_region["n"]),
                core.QgsPoint(grass_region["e"], grass_region["s"]),
                core.QgsPoint(grass_region["w"], grass_region["s"])]

    #polygon = [line]
    #g = core.QgsGeometry.fromPolygon(polygon)
    g = core.QgsGeometry.fromPolyline(line)
    f = core.QgsFeature()
    f.setGeometry(g)

    data_provider.addFeatures([f])
    layer.updateExtents()
    if emit is True:
        layer.dataChanged.emit()
        layer.repaintRequested.emit()


def grass_region_to_memory_layer():
    """
    Convert the current GRASS region into a QGIS memory layer

    :return: New memory layer
    """
    #uri = "Polygon?crs=epsg:4326"
    uri = "LineString?crs=epsg:4326"
    layer = core.QgsVectorLayer(uri, "Current GRASS region", "memory")

    wkt_proj = gscript.read_command("g.proj", flags="w")

    layer.setCrs(core.QgsCoordinateReferenceSystem(wkt_proj))
    layer.setReadOnly()

    grass_region = gscript.region()

    update_memory_layer_by_region(layer, grass_region, False)

    return layer


def main():
    layer = grass_region_to_memory_layer()

    core.QgsMapLayerRegistry.instance().addMapLayer(layer)

    grass_region = {}
    grass_region["w"] = 3500000
    grass_region["e"] = 3700000
    grass_region["n"] = 6000000
    grass_region["s"] = 5500000

    update_memory_layer_by_region(layer, grass_region)

