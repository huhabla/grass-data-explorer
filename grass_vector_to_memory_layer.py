# -*- coding: utf-8 -*-
"""
/***************************************************************************
 grass_vector_to_memory_layer
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import qgis.core as core
import grass.script as gscript
from PyQt4 import QtGui, QtCore


def grass_vector_to_memory_layer(grass_data_provider,
                                 grass_iface,
                                 iface,
                                 vector_name,
                                 mapset,
                                 feat_type="point",
                                 add_attr=False,
                                 bbox=None):
    """
    Convert a GRASS GIS vector layer into a QGIS memory layer
    using WKB functionality

    :param grass_data_provider:
    :param iface:
    :param vector_name:
    :param mapset:
    :param feat_type:
    :param add_attr:
    :param bbox:
    :return:
    """
    feat_type = feat_type.lower()
    table_dict = None

    #Check if the vector exsists
    try:
        info = grass_iface.read_vector_full_info(vector_name, mapset)
    except Exception, e:
        QtGui.QMessageBox.critical(None, "GRASS Fatal Error", "Fatal error occurred in GRASS: %s"%str(e))
        return None

    if not info:
        QtGui.QMessageBox.critical(None, "GRASS Fatal Error", "Unable to find GRASS vector layer: %s"%vector_name)
        return None

    # Get all attrs from the table and create dict with cat as keys
    col_string = ""
    table_dict = None
    if add_attr is True:
        # Get the database table as dict
        message = "grass_vector_to_memory_layer: Reading GRASS vector database, this may take a while."
        core.QgsMessageLog.logMessage(message)
        iface.mainWindow().statusBar().showMessage(message)
        ret = grass_data_provider.get_vector_table_as_dict(vector_name)

        if ret is not None:
            table_dict = ret["table"]
            columns = ret["columns"]

            if columns is not None:
                if feat_type != "node" and add_attr is True:
                    for name in columns.names():
                        data_type = columns[name].split()[0]
                        data_type = data_type.replace("varchar", "string")
                        data_type = data_type.replace("TEXT", "string")
                        col_string += "&field=%s:%s" % (name, data_type)

    # Setup the column names and types
    if feat_type == "point" or feat_type == "centroid":
        uri = "Point?crs=epsg:4326" + col_string + "&index=yes"
    elif feat_type == "line" or feat_type == "boundary":
        uri = "LineString?crs=epsg:4326" + col_string + "&index=yes"
    elif feat_type == "area":
        uri = "Polygon?crs=epsg:4326" + col_string + "&index=yes"

    layer = core.QgsVectorLayer(uri, vector_name +
                                "  (%s)" % (feat_type), "memory")

    wkt_proj = gscript.read_command("g.proj", flags="w")

    layer.setCrs(core.QgsCoordinateReferenceSystem(wkt_proj))

    # Data provider for efficient feature handling
    dataProvider = layer.dataProvider()
    # The field definitions
    fields = dataProvider.fields()
    # Get the WKB list

    message = "grass_vector_to_memory_layer: Reading GRASS vector as Well Known Binary, "\
              "this may take a while. Feature type: %s"%feat_type
    core.QgsMessageLog.logMessage(message)
    iface.mainWindow().statusBar().showMessage(message)
    wkb_list = grass_data_provider.get_vector_features_as_wkb_list(vector_name,
                                                                   mapset,
                                                                   bbox,
                                                        feature_type=feat_type)

    iface.mainWindow().statusBar().showMessage("Converting GRASS vector "
                                               "to memory layer <%s> %3.0f" %
                                               (vector_name, 0.0))
    # Iterate over the list and fill the QGIS vector with data
    if wkb_list and len(wkb_list) > 0:
        num_feat = len(wkb_list)
        count = 0
        featureList = []
        for entry in wkb_list:
            if int(num_feat / 20) > 0:
                if (num_feat - count) % int(num_feat / 20) == 0:
                    progress = 100.0 * (count + 1) / num_feat
                    message = "Converting GRASS vector "\
                              "to memory layer <%s> %3.0f" %\
                              (vector_name, progress)
                    core.QgsMessageLog.logMessage("grass_vector_to_memory_layer: " + message)
                    iface.mainWindow().statusBar().showMessage(message)

            f_id, cat, wkb = entry

            g = core.QgsGeometry()
            g.fromWkb(wkb)
            f = core.QgsFeature(fields)
            f.setGeometry(g)

            # Attribute data
            if table_dict is not None and cat is not None:
                f.setAttributes(table_dict[cat])

            featureList.append(f)
            count += 1

        dataProvider.addFeatures(featureList)

    message = "Converting GRASS vector "\
              "to memory layer <%s> %3.0f" % (vector_name, 100.0)
    core.QgsMessageLog.logMessage(message)
    iface.mainWindow().statusBar().showMessage(message)
    return(layer)
