# -*- coding: utf-8 -*-
"""
/***************************************************************************
 stvdsSettingsDialog
                                 A QGIS plugin
 Plugin to explorer GRASS GIS vector layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from qgis.core import *
from qgis.gui import *
from ui_stvds_settings import Ui_STVDSSettings
import grass.temporal as tgis
from grass_sample_tool import GrassSampleTool
# create the dialog for zoom to point
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class STVDSSettingsDialog(QtGui.QDialog, Ui_STVDSSettings):
    """
    This class manages all settings and analysis that can be
    performed on space time vector datasets.
    """
    def __init__(self,
                 iface,
                 stvds_layer,
                 feat_type,
                 add_attr=True,
                 bbox=None):

        QtGui.QDialog.__init__(self)

        self.setupUi(self)
        self.iface = iface
        self.canvas = self.iface.mapCanvas()
        self.stvds_layer = stvds_layer
        self.feat_type = feat_type
        self.add_attr = add_attr
        self.bbox = bbox
        self.map_name_dict = {} # To store the map names and treeWidget index

        self.stvdsSampleTool = None

        # Add layer name to window title
        title = self.windowTitle()
        self.setWindowTitle(title + " (%s)"%(self.stvds_layer.stvds_name))

        # Setup database connection
        self.dbif, self.connected = tgis.init_dbif(None)
        # Load the stvds
        self.stvds = tgis.open_old_stds(self.stvds_layer.stvds_name,
                                        "stvds", self.dbif)
        self.stvds.select(self.dbif)
        # Generate the maplist
        self.map_list = self.stvds.get_registered_maps(columns="name,start_"
                                                               "time,end_"
                                                               "time",
                                                       order="start_time",
                                                       dbif=self.dbif)
        if self.connected is True:
            self.dbif.close()

        self.plainTextEdit.setPlainText(self.stvds_layer.info_text)

        # Setup the toolbar
        self.toolBar = QtGui.QToolBar()
        self.verticalLayout.insertWidget(0, self.toolBar)

        # Connect buttons
        self.treeWidget.itemClicked.connect(self.switch_vector_layer)
        self.treeWidget.itemActivated.connect(self.switch_vector_layer)
        self.treeWidget.currentItemChanged.connect(self.switch_vector_layer)

        self.pushButtonListMaps.clicked.connect(self.generate_show_map_list)

        self.toolButtonLayerStyle.clicked.connect(self.load_layer_style_file)
        self.toolButtonLabelStyle.clicked.connect(self.load_label_style_file)

        # Show the stvds, stvds and map lists
        self.show_map_list()

        # Switching the stacked widgets in the dialog
        self.listWidget.itemClicked.connect(self.switch_stacked_widget)
        self.listWidget.itemActivated.connect(self.switch_stacked_widget)
        self.listWidget.currentItemChanged.connect(self.switch_stacked_widget)

        ################ Vector layer cache #################
        self.pushButtonApplyCacheSize.clicked.connect(self.apply_vector_layer_cache_size)
        self.pushButtonClearCache.clicked.connect(self.clear_vector_layer_cache)
        self.stvds_layer.vectorCacheChanged.connect(self.generate_show_cache_info)
        self.stvds_layer.layerNameChanged.connect(self.update_map_list_selection)

        ################ Vector Context Menu ################
        # Define the context menu when pressing the right mouse button on treeWidget
        self.treeWidget.customContextMenuRequested.connect(self.on_vector_context_menu)
        self.vectorContextMenu = QtGui.QMenu(self)
        self.vectorContextMenuAction = QtGui.QAction(u"Load as memory layer", self)
        self.vectorContextMenuAction.triggered.connect(self.call_vector_context_menu)
        self.vectorContextMenu.addAction(self.vectorContextMenuAction)

        ########################## Sampling #################
        # GRASS stvds QgsMapTool
        self.grass_vector_sample_tool = GrassSampleTool(self.canvas)
        self.grass_vector_sample_tool.canvasClicked.connect(self.sample_vector_point)

        # Create action for the raster map samppling
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/SampleVector.svg"),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stvdsSampleAction = QtGui.QAction(icon, u"Sample the current Vector Layer of Grass STVDS Layer", self)
        # connect the action to the run method
        self.stvdsSampleAction.triggered.connect(self.set_grass_vector_sample_tool)

        # Add toolbar button and menu item
        self.toolBar.addAction(self.stvdsSampleAction)

    def switch_stacked_widget(self):
        self.stackedWidget.setCurrentIndex(self.listWidget.currentIndex().row())

        if self.listWidget.currentIndex().row() == 3:
            self.set_grass_vector_sample_tool()

    ####################################################################
    ################# VECTOR MAP SAMPLING ##############################
    ####################################################################

    @QtCore.pyqtSlot()
    def set_grass_vector_sample_tool(self):
        """
        Set the grass sample MapTool and switch to the
        output tab.
        :return:
        """
        self.stackedWidget.setCurrentIndex(3)
        self.canvas.setMapTool(self.grass_vector_sample_tool)

    @QtCore.pyqtSlot(QgsPoint)
    def sample_vector_point(self, point):
        """
        Call the vector sample method from the grass sample tool
        and switch to the output tab.
        :param point: QgsPoint of coordinates in the GRASS location coordinate system
        :return:
        """

        if self.stvds_layer and self.stvds_layer.vector_name:
            distance = self.doubleSpinBoxDistance.value()
            table_id = self.spinBoxVectorTable.value()
            self.stackedWidget.setCurrentIndex(3)
            self.listWidget.setItemSelected(self.listWidget.item(3), True)
            line = self.grass_vector_sample_tool.sample_vectors_by_point(point,
                                                                        distance,
                                                                        table_id,
                                                                        [self.stvds_layer.vector_name,])

            self.textEditSampling.setText(line)

    ####################################################################
    ################# CONTEXT MENUES ###################################
    ####################################################################

    def call_vector_context_menu(self):
        item = self.treeWidget.currentItem()
        self.switch_vector_layer(item, 0)
        layer = self.stvds_layer.currentVectorLayer()
        # Deep copy the memory layer
        if layer:
            provider = layer.dataProvider()
            #provider = QgsVectorDataProvider()

            geo_type = provider.geometryType()

            if geo_type == 1:
                uri = "Point?crs=epsg:4326"
            elif geo_type == 2:
                uri = "LineString?crs=epsg:4326"
            elif geo_type == 3:
                uri = "Polygon?crs=epsg:4326"
            else:
                return

            new_layer = QgsVectorLayer(uri, layer.name(), "memory")
            new_layer.setCrs(layer.crs())

            new_provider = new_layer.dataProvider()
            for field in provider.fields():
                new_provider.addAttributes([field])

            feature_list = []

            for feature in provider.getFeatures():
                out_feat = QgsFeature()
                out_feat.setGeometry(feature.geometry() )
                out_feat.setAttributes(feature.attributes())
                feature_list.append(out_feat)

            new_provider.addFeatures(feature_list)
            new_layer.updateFields()
            new_layer.updateExtents()

            QgsMapLayerRegistry.instance().addMapLayer(new_layer)


    def on_vector_context_menu(self, point):
         self.vectorContextMenu.exec_( self.treeWidget.mapToGlobal(point))

    ####################################################################
    ################# VECTOR LAYER CACHE ###############################
    ####################################################################

    def apply_vector_layer_cache_size(self):
        """
        Apply the new vector layer cache size.
        The vector layer cache will be cleared
        when this function is applied
        :return:
        """
        size = self.spinBoxCacheSize.text()
        self.stvds_layer.set_max_layer_cache_size(int(size))

    def clear_vector_layer_cache(self):
        """
        Remove all vector layers from the internal cache
        :return:
        """
        self.stvds_layer.clear_layer_cache()

    def generate_show_cache_info(self):

        content = ""
        content += "Number of vector layers in memory cache:...... %i\n\n"%(len(self.stvds_layer.layer_cache))

        count = 1
        for layer in self.stvds_layer.layer_cache.values():
            content += "Layer %i\n"%(count)
            content += "  Name: %s\n"%(layer.name())
            content += "  Number of features: %i\n"%(layer.dataProvider().featureCount())
            count += 1

        self.textEditCacheInfo.setText(content)


    ####################################################################
    ################# VECOTR LAYER SWITCH AND STYLES ###################
    ####################################################################


    def load_layer_style_file(self):
        file_name = QtGui.QFileDialog.getOpenFileName(self, "Open Layer Style File",
                                                      "", "Qgis layer style files (*.qml)")
        self.lineEditLayerStyle.setText(file_name)
        self.stvds_layer.set_layer_style_file(file_name)

    def load_label_style_file(self):
        file_name = QtGui.QFileDialog.getOpenFileName(self, "Open Label Style File",
                                                      "", "Qgis label style files (*.qml)")
        self.lineEditLabelStyle.setText(file_name)
        self.stvds_layer.set_label_style_file(file_name)

    def switch_vector_layer(self, item, column):
        """
        Switch the vector map layer that will be visualized in the
        map canvas
        """
        if item is None:
            return
        vector_name = item.text(0)
        self.stvds_layer.change_map(vector_name,
                                    self.feat_type,
                                    self.add_attr,
                                    self.bbox)

        self.generate_show_cache_info()
        # Unset the map tool
        if self.stvdsSampleTool:
            self.iface.mapCanvas().unsetMapTool(self.stvdsSampleTool)
            self.stvdsSampleTool = None


    @QtCore.pyqtSlot()
    def generate_show_map_list(self):
        """
        Read the maps the a space time vector dataset using where and order
        statements and call the method to list them in the map list tree widget.
        :return:
        """

        self.map_list = []
        where = self.lineEditWhere.text()
        order = self.comboBoxOrder.currentText()

        try:
            dbif, connected = tgis.init_dbif(self.dbif)
            # Generate the maplist
            self.map_list = self.stvds.get_registered_maps(columns="name,start_time,end_time",
                                                           where=where,
                                                           order=order,
                                                           dbif=self.dbif)
            self.show_map_list()
        except Exception, e:
            QtGui.QMessageBox.critical(self, "Error", "Error in SQL:\n%s"%(str(e)))
            return
        finally:

            if connected is True:
                self.dbif.close()

    def show_map_list(self):
        """
        Fill the tree widgets with vector maps
        """

        self.map_name_dict.clear()
        self.treeWidget.clear()
        self.treeWidget.setColumnCount(3)
        self.treeWidget.setHeaderLabels(["Map name", "Start time",
                                         "End time"])

        if len(self.map_list) == 0:
            return

        count = 0
        for _map in self.map_list:

            map_item = QtGui.QTreeWidgetItem()
            map_item.setText(0, _map["name"])
            map_item.setText(1, str(_map["start_time"]))
            map_item.setText(2, str(_map["end_time"]))

            # Use the name as key and the item as value
            self.map_name_dict[_map["name"]] = map_item

            self.treeWidget.insertTopLevelItem(count, map_item)
            count += 1

        self.treeWidget.resizeColumnToContents(0)
        self.treeWidget.resizeColumnToContents(1)
        self.treeWidget.resizeColumnToContents(2)
        self.treeWidget.resizeColumnToContents(3)

    def update_map_list_selection(self):
        """
        Select the currently rendered map in the map list
        :return:
        """
        if self.stvds_layer.vector_name in self.map_name_dict:
            self.treeWidget.clearSelection()
            item = self.map_name_dict[self.stvds_layer.vector_name]
            self.treeWidget.setItemSelected(item, True)
