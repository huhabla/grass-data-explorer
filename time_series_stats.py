# -*- coding: utf-8 -*-
"""
/***************************************************************************
 TimeSeriesStatisticsComputation
                                 A QGIS plugin
 Plugin to explorer GRASS GIS raster layers, vector layers and
 space time datasets
                             -------------------
        begin                : 2015-09-02
        copyright            : (C) 2015 Thünen Institutes of Climate-Smart Agriculture
                               https://www.ti.bund.de/en/ak/
        email                : soerengebbert@googlemail.com
        author               : Sören Gebbert
                               https://www.ti.bund.de/en/ak/staff/wissenschaftliches-personal/dipl-ing-soeren-gebbert/
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
import grass.script as gscript
import csv
from datetime import datetime
from subprocess import PIPE
from grass.pygrass.modules import Module

# create the dialog for zoom to point
try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s


class TimeSeriesStatisticsComputation(QtCore.QObject):

    abort = QtCore.pyqtSignal()
    error = QtCore.pyqtSignal(Exception)
    # This signal sends the generated data
    finished = QtCore.pyqtSignal(dict, str)

    def __init__(self, strds, settings):
        """
        Constructor for time series computation
        :param strds: The space time raster dataset QGIS layer object
        :param settings: The widget that manages the settings
        :return:
        """
        self.where = None
        self.strds = strds
        self.settings = settings
        self.t_rast_univar = Module("t.rast.univar",
                                    flags="u",
                                    verbose=True,
                                    run_=False,
                                    stdout_=PIPE)
        super(TimeSeriesStatisticsComputation, self).__init__()

    def __del__(self):
        try:
            self.t_rast_univar.popen.kill()
        except Exception, e:
            self.abort.emit()
            self.error.emit(e)

    def __exit__(self):
        try:
            self.t_rast_univar.popen.kill()
        except Exception, e:
            self.abort.emit()
            self.error.emit(e)

    def set_values(self, where, dlg):
        """
        Set the initial values
        :param where:
        :param strds:
        :return:
        """
        self.where = where
        self.dlg = dlg

    @QtCore.pyqtSlot()
    def compute_stats(self):
        if self.strds is None:
            self.abort.emit()
            return None

        try:
            if self.t_rast_univar.popen:
                self.t_rast_univar.popen.kill()
        except Exception, e:
            gscript.error(e)

        try:
            # We need to use a temporary region to compute the statistics
            gscript.use_temp_region()
            if self.settings.checkBoxUseRasterRegions.isChecked() is True:
                self.t_rast_univar.flags.r = True
            else:
                self.t_rast_univar.flags.r = False
            self.t_rast_univar.inputs.input = self.strds.get_id()
            self.t_rast_univar.inputs.where = self.where
            print(self.t_rast_univar.get_bash())
            self.t_rast_univar.run()
            output = self.t_rast_univar.outputs.stdout
            gscript.del_temp_region()
        except Exception, e:
            print(e)
            self.abort.emit()
            self.error.emit(Exception("Error in t.rast.univar:\n%s"%(str(e))))
            return None

        pos = ['id', 'start', 'end', 'mean', 'min', 'max', 'mean_of_abs',
               'stddev', 'variance', 'coeff_var', 'sum', 'null_cells',
               'cells']

        temp_file = gscript.tempfile(1)
        out_file = open(temp_file, "w")
        out_file.write(output)
        out_file.close()

        r = csv.reader(open(temp_file, "r"), delimiter="|")

        mean = []
        _min = []
        _max = []
        stddev = []
        variance = []
        _sum = []
        pos = []
        dates = []
        date_strings = []

        first = True
        for e in r:
            if self.strds.get_temporal_type() == "absolute":
                if first is True:
                    first = datetime.strptime(e[1], "%Y-%m-%d %H:%M:%S")
                start = datetime.strptime(e[1], "%Y-%m-%d %H:%M:%S")
                delta = start - first
                pos.append(delta.total_seconds())
            else:
                start = int(e[1])
                pos.append(start)

            dates.append(start)
            date_strings.append(str(start))
            # Numerical values
            mean.append(float(e[3]))
            _min.append(float(e[4]))
            _max.append(float(e[5]))
            stddev.append(float(e[7]))
            variance.append(float(e[8]))
            _sum.append(float(e[10]))

        result = {}
        result["mean"] = mean
        result["min"] = _min
        result["max"] = _max
        result["dates"] = dates
        result["date_strings"] = date_strings
        result["pos"] = pos
        result["sum"] = _sum
        result["variance"] = variance
        result["stddev"] = stddev

        self.finished.emit(result, temp_file)
